# drone_server

Серверная часть и web-GCS
Управление станцией и дроном


Борт: https://bitbucket.org/nee77/drone_board/
Станция: https://bitbucket.org/nee77/drone_station/


# Установка

    sudo apt-get update
    sudo apt-get upgrade
    sudo apt-get install curl git


## Node JS

    curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
    sudo apt-get install -y nodejs build-essential


## Менеджер процессов PM2

    sudo npm install pm2 -g


## RethinkDB

    source /etc/lsb-release && echo "deb http://download.rethinkdb.com/apt $DISTRIB_CODENAME main" | sudo tee /etc/apt/sources.list.d/rethinkdb.list
    wget -qO- https://download.rethinkdb.com/apt/pubkey.gpg | sudo apt-key add -
    sudo apt-get update
    sudo apt-get install rethinkdb


Файл конфигурации RethinkDB

    sudo nano /etc/rethinkdb/instances.d/drone_server.conf

Останавливаем сервис

    sudo /etc/init.d/rethinkdb stop

Создать каталог для данных БД

    sudo rethinkdb create -d /var/lib/rethinkdb/drone_server
    sudo chown -R rethinkdb.rethinkdb /var/lib/rethinkdb/drone_server

Запускаем сервис

    sudo /etc/init.d/rethinkdb start


### Резервная копия

Горячая резервная копия

    cd db_backup
    rethinkdb dump -c localhost:28015

Восстановление из резервной копии

    rethinkdb restore backup.tar.gz -c localhost:28015



## Redis

    sudo apt-get install redis-server
    sudo /etc/init.d/redis-server status

Файл конфигурации

    sudo nano /etc/redis/redis.conf

Перезапуск процесса

    sudo systemctl restart redis



## Nginx

Установка

    sudo apt-get install nginx

Файлы сайтов

    cd /etc/nginx/sites-available
    sudo nano SITE-NAME.COM


Включить сайт

    sudo ln -s /etc/nginx/sites-available/SITE-NAME.COM /etc/nginx/sites-enabled/SITE-NAME.COM

Управление nginx

    sudo systemctl status nginx
    sudo systemctl restart nginx



## SSL certificates

https://certbot.eff.org/lets-encrypt/ubuntuxenial-nginx

Установка

    sudo apt-get update
    sudo apt-get install software-properties-common
    sudo add-apt-repository ppa:certbot/certbot
    sudo apt-get update
    sudo apt-get install python-certbot-nginx

Создать сертификат

    sudo certbot --nginx

Обновление сертификатов

    sudo certbot renew --dry-run





## drone_server

    git clone https://nee77@bitbucket.org/nee77/drone_server.git
    cd drone_server
    npm install
    npm run build

Конфигурация

    nano config.js


## Запуск

    pm2 start server.js



## Включение автозапуска

    pm2 save
    pm2 startup



## Обновление

    git pull https://nee77@bitbucket.org/nee77/drone_server.git
    npm install
    npm run build





