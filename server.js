const config = require('./config');
const users_db = require('./users');


const http = require('http');
const express = require('express');
const session = require('express-session');
const RedisStore = require('connect-redis')(session);
const bodyParser = require('body-parser');
const app = express();
const server = new http.Server(app);

const crypto = require('crypto');

const DroneModel = require('./utils/Drone');


// Redis client init
const redis = require('redis');
const redisClient = redis.createClient({ host: config.REDIS_HOST, port: config.REDIS_PORT });
redisClient.on('ready',function() { console.log("Redis is ready"); });
redisClient.on('error',function() { console.log("Error in Redis"); });

// Socket.io_server server init
const io_server = require('socket.io')(server);

// Redis session init
app.use(session({
    store: new RedisStore({host: config.REDIS_HOST, port: config.REDIS_PORT})
    ,name: 'usid'
    ,secret: config.SESSION_SECRET
    ,cookie: {maxAge: 10*3600*1000} // default { path: '/', httpOnly: true, secure: false, maxAge: null } (domain, expires, httpOnly, maxAge, path, secure, sameSite)
    ,proxy: true
    ,resave: true
    ,rolling: true
    ,saveUninitialized: false
    ,genid: function(req) {
        return crypto.randomBytes(48).toString('hex');
    }
}));

// static files init
app.use(express.static(__dirname + '/web-ui'));


// JSON parse init
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));


// API init

// sign in
app.post('/api/login', function (req, res) {

    let user = req.body.user;
    let pass = req.body.pass;

    if( !user || !pass ){
        res.json(null);
        return;
    }

    for( let i = 0, k = users_db.length; i < k; i++ ){
        if( users_db[i][0] === user && users_db[i][1] === pass ){

            req.session.login = true;
            req.session.user = users_db[i][0];
            req.session.io_key = crypto.randomBytes(16).toString('hex');
            req.session.hit = 1;

            redisClient.set('io_key_' + req.session.io_key, req.session.user, 'EX', 10*3600); // лимит сессии 3600 сек = 1 час

            res.json({
                user: users_db[i][0]
                ,io_key: req.session.io_key
            });

            return;
        }
    }

    res.json(null);

});


// check status
app.get('/api/login', function (req, res) {
    if( req.session.login  ){
        req.session.hit++;
        res.json({
            user: req.session.user
            ,io_key: req.session.io_key
        });
    }
    else {
        req.session.destroy();
        res.status(401).json({status: 'unauthorized'});
    }
});


// log OUT
app.post('/api/logout', function (req, res){

    redisClient.del('io_key_' + req.session.io_key);

    req.session.destroy();

    res.json(null);

});



// История статусов дрона
app.get('/api/drone/:drone_id/statuses', function (req, res){

    res.json([]);

});






app.get('/api/test', function (req, res){

    //const point = r.point(39,43);

    //r.table('Pilots').indexCreate('loc_coords', {geo: true}).run(function(err){
        //console.log(err);
        //console.log(r.table('Pilots').indexList());

        //r.table('Pilots').indexList().run(function(result){
        //    res.json(result);
        //});

        //
        //Pilot.getNearest(point, 25, 500).then(function(results){
            // results[i].dist, results[i].doc
        //    res.json(results);
        //});

    //});


    res.json({test: 'test'});

});




//
// Запуск сервера
server.listen(config.WEB_SERVER_PORT, () => {
    console.log('Listening on port ' + config.WEB_SERVER_PORT);
});


let drones = [];

//
// Соединение с сервером Socket.io
io_server.on('connection', function(io_client) {

    // Подключаются разные типы клиентов: дрон, станция, браузер
    let me = io_client.handshake.query.me;

    let drone_status_interval = null;

    if( !me ){
        io_client.disconnect(true);
        return;
    }

    io_client.on('disconnect', function () {
        console.log(me + ' disconnected');
    });


    // Подключен дрон
    if( 'drone' === me ){
        let drone_id = io_client.handshake.query.drone_id;

        if( drone_id && drone_id === config.DRONE_ID ){

            console.log('drone connected');

            let drone = new DroneModel(io_client, {
                id: config.DRONE_ID
                ,sysid: config.DRONE_SYSID
                ,compid: config.DRONE_COMPID
                ,mav_v: config.DRONE_MAVLINK_VERSION
                ,mav_msg_def: config.DRONE_MAVLINK_MSG_DEF
            });

            drones.push(drone);
            drone.start_telemetry('gcs');

            io_client.on('disconnect', function () {
                drone.disconnect();
            });

        }


        /*
        if( drone_id && drone_id === config.DRONE_ID ){
            console.log('drone connected ' + io_client.handshake.address);

            // Подключение к каналу drone
            io_client.join('drone');

            // Перенаправляем сообщения от дрона
            io_client.on('telem1', function(data){
                io_client.to('gcs').emit('telem1_' + drone_id, data);
                redisClient.set('last_telem_drone_' + drone_id, (new Date()).getTime());
            });

            io_client.on('telem2', function(data){
                io_client.to('gcs').emit('telem2_' + drone_id, data);
            });

            io_client.on('telem3', function(data){
                io_client.to('gcs').emit('telem3_' + drone_id, data);
            });

            io_client.on('status_text', function(data){
                io_client.to('gcs').emit('status_text_' + drone_id, data);
            });

            io_client.on('info', function(data){
                io_client.to('gcs').emit('info_' + drone_id, data);
            });

            io_client.on('com_ack', function(data){
                io_client.to('gcs').emit('com_ack_' + drone_id, data);
            });


        }
        */

    }

    // Подключена станция
    else if( 'station' === me ){
        let io_key = io_client.handshake.query.io_key;

        if( io_key === config.STATION_IO_KEY ){
            console.log('station connected ' + io_client.handshake.address);

            io_client.join('station');

            // Перенаправляем время станции
            io_client.on('station_time', function(data){
                io_client.to('gcs').emit('station_time', data);
            });

            // Перенаправляем статус станции
            io_client.on('station_status', function(data){
                io_client.to('gcs').emit('station_status', data);
            });

            // Перенаправляем информацию о зарядке
            io_client.on('charge', function(data){
                io_client.to('gcs').emit('charge', data);
            });
        }


    }

    // Подключен браузер
    else if( 'gcs' === me ){

        let io_key = io_client.handshake.query.io_key;
        let io_user = io_client.handshake.query.user;

        if( io_key && io_user ){
            redisClient.get('io_key_' + io_key, function(err, user) {
                if ( err || !user || user !== io_user ) {
                    io_client.emit('status', 'unauthorized');
                    io_client.disconnect(true);
                }
                else {
                    console.log('GCS connected ' + io_client.handshake.address);

                    io_client.join('gcs');

                    //for( let i = 0, k = drones.length; i < k; i++ ){
                        //drones[i].start_telemetry('gcs');
                    //}

                    // Перенаправляем команду на станцию
                    io_client.on('station_command', function(data){
                        io_client.to('station').emit('command', data);
                    });

                    // Перенаправляем команду на дрон
                    io_client.on('drone_command', function(msg){

                        console.log('IO cmd');
                        console.log(msg.data);

                        if( !msg || !msg.drone_id || !msg.data.command || !DroneModel.get(msg.drone_id) ) return;

                        DroneModel.get(msg.drone_id).command(msg.data);

                    });

                    io_client.on('disconnect', function () {
                        //for( let i = 0, k = drones.length; i < k; i++ ){
                        //    drones[i].stop_telemetry();
                        //}
                    });



                }
            });
        }

    }

    // Ни один из вышеперечисленных
    else io_client.disconnect(true);


});


//
// остановка скрипта
process.on('SIGINT', function() {
    /*
        процедуры перед остановкой скрипта
     */

    process.exit();

});

