import "./styles/app.css";
import "./styles/flightindicators.css";

import {JetApp, StoreRouter, plugins} from "webix-jet";
import session from "./models/session";



// После загрузки Webix
webix.ready( () => {

	// конструктор приложения
	const app = new JetApp({
		start: '/app/drone'
        ,router: StoreRouter
        ,debug: true
	});

	app.use( plugins.User, { model : session });

    // Обработка ошибок webix ajax
    webix.attachEvent("onLoadError", function(text){ // , xml, ajax, owner
        console.log('data load error: ' + text);

        const resp = JSON.parse(text);

        if( resp && resp.status ){
            if( 'unauthorized' === resp.status ){
                app.getService('user').logout();
            }
        }

    });

    // Обработчики ошибок webix jet
	app.attachEvent("app:error:resolve", function(name, error){
		window.console.error(error);
	});
	app.attachEvent("app:error:initview", function(view, error){
		window.console.error(error);
	});
	app.attachEvent("app:error:server", function(error){
		window.console.error(error);
	});


	// Рендер приложения
    app.render().then(function(){
        console.log('App rendered');
    });

});


//
// Горизонт
webix.protoUI({
    name: "fi_horizon" // the name of a new component
    ,defaults:{
        width: 200
        ,size: 200
        //,on:{'onItemClick' : function(){}} //attached events
    },
    $init: function(config){
        let img_dir = 'static/fi/';
        this.$view.innerHTML = '<div class="instrument attitude"><div class="roll box"><img src="' + img_dir + 'horizon_back.svg" class="box" alt="" /><div class="pitch box"><img src="' + img_dir + 'horizon_ball.svg" class="box" alt="" /></div><img src="' + img_dir + 'horizon_circle.svg" class="box" alt="" /></div><div class="mechanics box"><img src="' + img_dir + 'horizon_mechanics.svg" class="box" alt="" /><img src="' + img_dir + 'fi_circle.svg" class="box" alt="" /></div></div>';

        const _this = this;

        this.$ready.push(function(){
            let el = _this.$view.querySelectorAll("div.instrument")[0];
            if( el ){
                el.style.width = config.size + 'px';
                el.style.height = config.size + 'px';
            }
        });
    }

    ,setRoll: function(roll){
        let el = this.$view.querySelectorAll('div.instrument.attitude div.roll')[0];
        if( el ){
            el.style.transform = 'rotate(' + -roll + 'deg)';
        }
    }

    ,setPitch: function(pitch){
        let el = this.$view.querySelectorAll('div.instrument.attitude div.roll div.pitch')[0];

        if( pitch > 30 ){ pitch = 30; }
        else if( pitch < -30 ){ pitch = -30; }

        if( el ){
            el.style.top = pitch*0.7 + '%';
        }
    }

    /*
    ,$getSize: function(x, y){}
    ,$setSize:function(x, y){
        // this.$view.childNodes[i].style.width = ''
    }
    */
}, webix.ui.view);


// Компас
webix.protoUI({
    name: "fi_compass" // the name of a new component
    ,defaults:{
        width: 200
        ,size: 200
        //,on:{'onItemClick' : function(){}} //attached events
    },
    $init: function(config){
        let img_dir = 'static/fi/';
        this.$view.innerHTML = '<div class="instrument heading"><div class="heading box"><img src="' + img_dir + 'heading_yaw.svg" class="box" alt="" /></div><div class="mechanics box"><img src="' + img_dir + 'heading_mechanics.svg" class="box" alt="" /><img src="' + img_dir + 'fi_circle.svg" class="box" alt="" /></div></div>';

        const _this = this;

        this.$ready.push(function(){
            let idiv = _this.$view.querySelectorAll("div.instrument")[0];
            if( idiv ){
                idiv.style.width = config.size + 'px';
                idiv.style.height = config.size + 'px';
            }
        });
    }

    ,setHeading: function(heading){
        let el = this.$view.querySelectorAll('div.instrument.heading div.heading')[0];

        if( el ){
            el.style.transform = 'rotate(' + -heading + 'deg)';
        }

    }

    /*
    ,$getSize: function(x, y){}
    ,$setSize:function(x, y){
        // this.$view.childNodes[i].style.width = ''
    }
    */
}, webix.ui.view);


