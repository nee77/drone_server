//
// Делает читаемое время из кол-ва секунд
const timeFormat2 = function(seconds){
	let h = Math.floor( seconds/3600 );
	let m = '0' + Math.floor( (seconds - h*3600)/60 );
	let s = '0' + (seconds - h*3600 - m*60);

    return h + ':' + m.substr(-2) + ':' + s.substr(-2);
};

export default timeFormat2;