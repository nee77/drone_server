function status(){
	return webix.ajax().get("/api/login")
		.then(a => a.json());
}

function login(user, pass){
	return webix.ajax().post("/api/login", {
		user: user, pass: pass
	}).then(a => a.json() );
}

function logout(){
    return webix.ajax().post("/api/logout")
		.then(a => a.json());
}

export default {
	status, login, logout
}