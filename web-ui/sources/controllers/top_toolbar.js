import timeformat from "../utils/timeformat";

import StatusesCollection from './../models/StatusesCollection';


export default {

    init: function(view, url){
        view.$scope.app.setService("top_drone_time", {
            set: function(time){
                view.$scope.$$('tpl:drone_time').setValues({drone_time: timeformat(time)});
            }
        });
    }

    ,ready: function(view, url){
        const socket_io = window.socket_io;

        socket_io.on('station_time', function (data) {
            view.$scope.$$('tpl:station_time').setValues({station_time: timeformat(data)});
        });




    }

};