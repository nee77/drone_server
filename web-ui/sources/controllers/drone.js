import timeformat2 from '../utils/timeformat2';

const socket_io = window.socket_io;


const STATUSES_LIST_LIMIT = 50;

// Параметры маркера на карте
const marker_icon_params = {
    path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
    ,scale: 5
    ,strokeColor: '#160e01'
    ,fillColor: '#eede00'
    ,fillOpacity: 1.0
    ,strokeWeight: 3
    ,rotation: 180
};

let player = null;

let video_test_url = 'ws://video.roboflot.ru:8080/test11011/drone1/mse_ld';

//import FlussonicMsePlayer from '../utils/FlussonicMsePlayer';

//import FlussonicMsePlayer from '../utils/video_player';

const MAV = {

    AUTOPILOT: [
        'Generic 1' // full support for everything
        ,'Reserved'
        ,'SLUGS'
        ,'ArduPilot'
        ,'OpenPilot'
        ,'Generic 2' // autopilot only supporting simple waypoints
        ,'Generic 3' // autopilot supporting waypoints and other simple navigation commands
        ,'Generic 4' // autopilot supporting the full mission command set
        ,'GCS or MAVLink component' // No valid autopilot, e.g. a
        ,'PPZ UAV'
        ,'UAV Dev Board'
        ,'FlexiPilot'
        ,'PX4 Autopilot'
        ,'SMACCMPilot'
        ,'AutoQuad'
        ,'Armazila'
        ,'Aerob'
        ,'ASLUAV'
        ,'SmartAP Autopilot'
        ,'AirRails'
    ]

    ,TYPE: [
            'Generic MAV'
            ,'Fixed wing aircraft'
            ,'Quadrotor'
            ,'Coaxial helicopter'
            ,'Normal helicopter w/TR'
            ,'Ground installation'
            ,'Operator CU / GCS'
            ,'Airship'
            ,'Free balloon'
            ,'Rocket'
            ,'Ground rover'
            ,'Surface vessel, boat, ship'
            ,'Submarine'
            ,'Hexarotor'
            ,'Octorotor'
            ,'Tricopter'
            ,'Flapping wing'
            ,'Kite'
            ,'Onboard companion'
            ,'Two-rotor VTOL'
            ,'Quad-rotor VTOL'
            ,'Tiltrotor VTOL'
            ,'VTOL reserved 2'
            ,'VTOL reserved 3'
            ,'VTOL reserved 4'
            ,'VTOL reserved 5'
            ,'Onboard gimbal'
            ,'Onboard ADSB peripheral'
            ,'Steerable airfoil'
            ,'Dodecarotor'
            ,'Camera'
            ,'Charging station'
            ,'Onboard FLARM CAS'
    ]

    ,CUSTOM_MODE: {

        PLANE: {
             0: 'MANUAL'
            ,1: 'CIRCLE'
            ,2: 'STABILIZE'
            ,3: 'TRAINING'
            ,4: 'ACRO'
            ,5: 'FLY BY WIRE_A'
            ,6: 'FLY BY WIRE_B'
            ,7: 'CRUISE'
            ,8: 'AUTOTUNE'
            ,10: 'AUTO'
            ,11: 'RTL'
            ,12: 'LOITER'
            ,14: 'AVOID ADSB'
            ,15: 'GUIDED'
            ,16: 'INITIALIZING'
            ,17: 'QSTABILIZE'
            ,18: 'QHOVER'
            ,19: 'QLOITER'
            ,20: 'QLAND'
            ,21: 'QRTL'
        }


        ,COPTER: {
             0: 'STABILIZE'
            ,1: 'ACRO'
            ,2: 'ALT HOLD'
            ,3: 'AUTO'
            ,4: 'GUIDED'
            ,5: 'LOITER'
            ,6: 'RTL'
            ,7: 'CIRCLE'
            ,9: 'LAND'
            ,11: 'DRIFT'
            ,13: 'SPORT'
            ,14: 'FLIP'
            ,15: 'AUTOTUNE'
            ,16: 'POSHOLD'
            ,17: 'BRAKE'
            ,18: 'THROW'
            ,19: 'AVOID ADSB'
            ,20: 'GUIDED NOGPS'
            ,21: 'SMART RTL'
        }


        ,ROVER: {
             0: 'MANUAL'
            ,1: 'ACRO'
            ,3: 'STEERING'
            ,4: 'HOLD'
            ,5: 'LOITER'
            ,10: 'AUTO'
            ,11: 'RTL'
            ,12: 'SMART_RTL'
            ,15: 'GUIDED'
            ,16: 'INITIALIZING'
        }


        ,SUB: {
             0: 'STABILIZE'
            ,1: 'ACRO'
            ,2: 'ALT_HOLD'
            ,3: 'AUTO'
            ,4: 'GUIDED'
            ,7: 'CIRCLE'
            ,9: 'SURFACE'
            ,16: 'POSHOLD'
            ,19: 'MANUAL'
        }

    }

};


/*

    Класс Drone

 */
class Drone {

    constructor(drone_id){   //console.log('drone constructor');

        // Инфо о дроне
        let info = {
            type: null //
            ,autopilot: null //
        };

        const _this = this;
        this.drone_id = drone_id;
        this.view = null;
        this.online = false;

        // Телеметрия
        this.telem_1hz = new webix.DataRecord();
        this.telem_10hz = new webix.DataRecord();
        // Текстовые значения состояния
        this.info = new webix.DataRecord({
            data: {
                type_str: 'UNKNOWN'
                ,autopilot_str: 'UNKNOWN'
            }
        });

        this.player = null;

        this.modes = null;


        // Коллекция статусов
        this.statuses_collection = new webix.DataCollection({
            url: '/api/drone/' + this.drone_id + '/statuses'

            ,on: {
                'onAfterAdd': function () { // id
                    if( this.count() > STATUSES_LIST_LIMIT ){
                        this.remove(this.getLastId());
                    }
                }
            }

            ,scheme:{
                $init: function(obj){
                    if( obj.severity <= 3 ) obj.$css = "list_red";
                    else if( obj.severity <= 5 ) obj.$css = "list_yel";
                    else obj.$css = "list_white";
                }
            }
        });

        //
        // Маркер на карте
        this.marker = new google.maps.Marker({
            position: { lat:0, lng:0 }
            ,icon: marker_icon_params
        });

        // Функция отрисовки и перемещения маркера
        // coords: {lat,lng}, heading
        this.set_marker_position = function(){
            let t = _this.telem_1hz.getValues();

            // Обновляем положение маркера
            _this.marker.setPosition({lat: t.lat, lng: t.lon});

            // Поворот маркера
            let t10 = _this.telem_10hz.getValues();

            let heading = 0;
            if( t10.y < 0 ) heading = 360 + t10.y;
            else heading = t10.y;

            marker_icon_params.rotation = heading;

            _this.marker.setIcon(marker_icon_params);

        };

        this.set_marker_rotation = function(){
            let t = _this.telem_10hz.getValues();

            let heading = 0;
            if( t.y < 0 ) heading = 360 + t.y;
            else heading = t.y;

            // Поворот маркера
            marker_icon_params.rotation = heading;
            _this.marker.setIcon(marker_icon_params);

        };

        this.set_marker_map = function(){
            if( !_this.view ) return;

            const map = _this.view.$scope.$$('map:drone').getMap();

            let t = _this.telem_1hz.getValues();

            if( !_this.marker.getMap() && (t.lat && t.lon) ){
                _this.marker.setMap(map);

                map.panTo(_this.marker.getPosition());
                map.setZoom(13);
            }

        };

        //
        // Отправка высокоуровневых команд для предобработки на сервере
        this.command = function(command, params){

            if( !params ) params = {};

            //console.log(command);

            // Список исполняемых команд MAV_CMD
            const MAV_CMD = {
                takeoff: 22
                ,land: 21
                ,switch_relay: 181
                ,set_mode: 11 // MAV_CMD_DO_SET_MODE
            };

            // Отправляем команду
            socket_io.emit('drone_command', {
                drone_id: _this.drone_id
                ,data: {
                    command: command
                    ,params: params
                }
            });

            if( MAV_CMD.hasOwnProperty(command) ) {

                return new Promise(function (resolve, reject) {

                    let command_timeout = null;

                    // Если не будет получен ответ через 2 секунды, то вернуть ошибку таймаута
                    command_timeout = setTimeout(function () {
                        _this.command_ack.clear(MAV_CMD[command]);
                        //console.log('timeout');
                        reject('timeout');
                    }, 2000);

                    // Включить ожидание ответа на команду
                    _this.command_ack.wait(MAV_CMD[command], function (result) { // MAV_RESULT
                        // отмена таймаута
                        clearTimeout(command_timeout);

                        //console.log('got result for ' + command + ' = ' + result);

                        // Команда выполнена успешно MAV_RESULT_ACCEPTED
                        if (0 === result) {
                            _this.command_ack.clear(MAV_CMD[command]);
                            resolve('success');
                        }
                        // MAV_RESULT_TEMPORARILY_REJECTED
                        else if (1 === result) {
                            _this.command_ack.clear(MAV_CMD[command]);
                            reject('rejected');
                        }
                        // MAV_RESULT_DENIED
                        else if (2 === result) {
                            _this.command_ack.clear(MAV_CMD[command]);
                            reject('denied');
                        }
                        // MAV_RESULT_UNSUPPORTED
                        else if (3 === result) {
                            _this.command_ack.clear(MAV_CMD[command]);
                            reject('unsupported');
                        }
                        // MAV_RESULT_FAILED
                        else if (4 === result) {
                            _this.command_ack.clear(MAV_CMD[command]);
                            reject('failed');
                        }
                        // MAV_RESULT_IN_PROGRESS
                        else if (5 === result) {
                            resolve('in_progress');
                        }
                    });

                    //console.log('sending command ' + command);

                });
            }
        };

        //
        // Хранение и обработка подверждений команд
        this.command_ack = function(){

            let list = {};

            return {
                wait: function(command_id, callback){
                    list[command_id] = callback;
                }
                ,set: function(command_id, result){
                    if( list[command_id] ){
                        list[command_id](result);
                    }
                }
                ,clear: function(command_id) {
                    if( list[command_id] ){
                        list[command_id] = undefined;
                    }
                }
            };
        }();

        // Статус вида онлайн или оффлайн
        this.view_status = function(){

            return {
                online: function(){
                    if( _this.view && !_this.view.isEnabled() ){
                        _this.view.enable();
                        webix.message('Дрон на связи');
                        const map = _this.view.$scope.$$('map:drone').getMap();
                        _this.set_marker_map(map);
                    }
                }
                ,offline: function(){
                    if( _this.view && _this.view.isEnabled() ){
                        // FIXME
                        _this.view.disable();
                        webix.message({text: 'Дрон не на связи', type: 'warning'});
                    }
                }
            };
        }();


        // Прием сообщений с сервера
        socket_io.on('telem_1hz_' + this.drone_id, telem_1hz => {
            _this.telem_1hz.setValues({last_timestamp: Math.round((new Date()).getTime()/1000)}, true);
            _this.telem_1hz.setValues(telem_1hz, true);

            let mode = '';
            let custom_mode = false;
            let armed = 0;

            if( 128 & telem_1hz.b_mode ) armed = 1;
            if( 1 & telem_1hz.b_mode ) custom_mode = true;
            if( 2 & telem_1hz.b_mode ) mode = mode + ' TEST';
            if( 4 & telem_1hz.b_mode ) mode = mode + ' AUTO';
            if( 8 & telem_1hz.b_mode ) mode = mode + ' GUIDED';
            if( 16 & telem_1hz.b_mode ) mode = mode + ' STABILIZE';
            if( 32 & telem_1hz.b_mode ) mode = mode + ' HIL';
            if( 64 & telem_1hz.b_mode ) mode = mode + ' MANUAL';


            // Вычисление полетного режима
            if( custom_mode ){

                mode = '';

                let type = _this.info.getValues().type;
                if( type ){ // MAV_TYPE
                    // Aircraft
                    if( [1, 7, 16].indexOf(type) !== -1 ){
                        mode = mode + ' ' + MAV.CUSTOM_MODE.PLANE[telem_1hz.c_mode];
                    }
                    // Copter
                    else if( [2, 3, 4, 13, 14, 15, 19, 20, 21].indexOf(type) !== -1 ){
                        mode = mode + ' ' + MAV.CUSTOM_MODE.COPTER[telem_1hz.c_mode];
                    }
                    // Rover
                    else if( [10, 11].indexOf(type) !== -1 ){
                        mode = mode + ' ' + MAV.CUSTOM_MODE.ROVER[telem_1hz.c_mode];
                    }
                    // Sub
                    else if( [12].indexOf(type) !== -1 ){
                        mode = mode + ' ' + MAV.CUSTOM_MODE.SUB[telem_1hz.c_mode];
                    }

                    // Other
                    else {
                        mode = mode + ' ' + telem_1hz.c_mode;
                    }

                }
                else {
                    mode = mode + ' ' + telem_1hz.c_mode;
                }
            }




            _this.telem_1hz.setValues({
                 mode: mode
                ,armed: armed
            }, true);

            _this.info.setValues({
                time_on: timeformat2(telem_1hz.time_b)
            }, true);

            _this.set_marker_position();

            // Обновить информацию на экране
            if( _this.view ){
                let t1 = _this.telem_1hz.getValues();

                webix.$$('dvt:lbl:armed').setValue( armed ? 'ARMED' : 'Disarmed' );

                webix.$$('dvt:rs:mode').blockEvent();
                if( webix.$$('dvt:rs:mode').isEnabled() ) webix.$$('dvt:rs:mode').setValue( telem_1hz.c_mode );
                webix.$$('dvt:rs:mode').unblockEvent();

                //webix.$$('dvt:tpl:time').setValues(t1);

                _this.view.$scope.fi_popup.queryView({localId:'tpl:telem'}).setValues(telem_1hz, true);

                if( !_this.marker.getMap() ) _this.set_marker_map();

                if( armed ){
                    webix.$$('dvt:btn:arm').hide();
                    webix.$$('dvt:btn:disarm').show();
                }
                else {
                    webix.$$('dvt:btn:disarm').hide();
                    webix.$$('dvt:btn:arm').show();
                }

                //console.log( _this.telem_1hz.getValues() );

            }

        });

        socket_io.on('telem_10hz_' + this.drone_id, telem_10hz => {
            _this.telem_10hz.setValues(telem_10hz, true);

            if( _this.view ){
                let horizon = _this.view.$scope.fi_popup.queryView({localId: 'fi:horizon'});
                let compass = _this.view.$scope.fi_popup.queryView({localId: 'fi:compass'});

                horizon.setRoll(telem_10hz.r);
                horizon.setPitch(telem_10hz.p);

                compass.setHeading(telem_10hz.y);

                _this.view.$scope.fi_popup.queryView({localId:'tpl:telem'}).setValues(telem_10hz, true);

                _this.set_marker_rotation();

            }



        });

        //
        // Статусы
        socket_io.on('status_text_' + this.drone_id, data => {
            this.statuses_collection.add(data, 0);
            //console.log(data);
        });

        //
        // Прием сообщений с сервера
        socket_io.on('info_' + this.drone_id, info => {
            _this.info.setValues(info, true);

            _this.info.setValues({
                type_str: MAV.TYPE[info.type]
                ,autopilot_str: MAV.AUTOPILOT[info.autopilot]
                ,last_timestamp: (new Date()).getTime()
            }, true);


            if( !_this.modes ){

                _this.modes = [];

                let type = info.type;
                let frame_modes = null;

                // Aircraft
                if( [1, 7, 16].indexOf(type) !== -1 ){
                    frame_modes = MAV.CUSTOM_MODE.PLANE;
                }
                // Copter
                else if( [2, 3, 4, 13, 14, 15, 19, 20, 21].indexOf(type) !== -1 ){
                    frame_modes = MAV.CUSTOM_MODE.COPTER;
                }
                // Rover
                else if( [10, 11].indexOf(type) !== -1 ){
                    frame_modes = MAV.CUSTOM_MODE.ROVER;
                }
                // Sub
                else if( [12].indexOf(type) !== -1 ){
                    frame_modes = MAV.CUSTOM_MODE.SUB;
                }


                if( frame_modes ){
                    for( let m_id in frame_modes ){
                        if( frame_modes.hasOwnProperty(m_id) ) _this.modes.push({id: m_id, value: frame_modes[m_id]});
                    }
                }
                else {
                    _this.modes.push({id: 0, value: 'Default'});
                }

                if( _this.view && webix.$$('dvt:rs:mode') ){
                    webix.$$('dvt:rs:mode').getList().parse(_this.modes);

                }

            }


        });

        //
        // Сообщение с подтверждением исполнения команды
        socket_io.on('com_ack_' + this.drone_id, data => {
            // Передается в ожидающую функцию
            _this.command_ack.set(data.command, data.result);  // MAV_CMD, MAV_RESULT
            console.log('com ack ');
            console.log(data);
        });


        socket_io.on('ping_' + this.drone_id, data => {
            console.log('ping ' + data);
        });

        //
        // Проверка на связи дрон или нет
        setInterval(function(){
            let time_now = Math.round((new Date()).getTime()/1000);
            let last_telem = _this.telem_1hz.getValues().last_timestamp;

            //console.log(last_telem);

            if( !last_telem || time_now > last_telem + 5 ){
                _this.view_status.offline();
                _this.online = false;
            }
            else {
                _this.view_status.online();
            }
        }, 1000);

    }


    //
    // Включение обновления вида своими данными
    view_start(view){   //console.log('drone view' + this.drone_id);
        this.view = view;
        const _this = this;

        console.log('view_start 1');

        this.command('info');

        //
        //  Объекты вида
        //
        // Список статусов
        const statuses_list = view.$scope.statuses_popup.queryView({localId: 'list:statuses'});
        // Объект карты Google
        const map = view.$scope.$$('map:drone').getMap();

        // Меню выбора полетного режима
        //const mode_select = view.$scope.action_menu.queryView({localId: 'rs:mode'});
        const mode_select = webix.$$('dvt:rs:mode');

        // Кнопка Взлет
        //const button_takeoff = view.$scope.action_menu.queryView({localId: 'btn:takeoff'});
        // Шаблон данных телеметрии
        //const telem_view = view.$scope.$$('data:telem');

        // Выключатели реле
        const relay1_switch = view.$scope.action_menu.queryView({localId: 'sw:rel1'});
        const relay2_switch = view.$scope.action_menu.queryView({localId: 'sw:rel2'});
        const relay3_switch = view.$scope.action_menu.queryView({localId: 'sw:rel3'});
        const relay4_switch = view.$scope.action_menu.queryView({localId: 'sw:rel4'});

        // Плеер и видеошум
        const video_player_tpl = view.$scope.fi_popup.queryView({localId:'tpl:video_player'});
        const video_noise_tpl = view.$scope.fi_popup.queryView({localId:'tpl:video_noise'});

        // Шаблон информации
        const popup_info_tpl = view.$scope.info_popup.queryView({localId: 'tpl:info'});

        const telem_tpl = view.$scope.fi_popup.queryView({localId:'tpl:telem'});


        if( _this.modes ){
            mode_select.getList().parse(_this.modes);
        }

        mode_select.attachEvent('onChange', function(new_value, old_value){
            mode_select.disable();

            _this.command('set_mode', {
                base_mode: 1
                ,custom_mode: new_value
            }).then(function(res){
                webix.message('Режим установлен');
                setTimeout(function() {
                    mode_select.enable();
                }, 1500);
            }).catch(function(res){
                webix.message({text: 'Режим не установлен', type: 'error'});

                mode_select.blockEvent();
                mode_select.setValue(old_value);
                mode_select.unblockEvent();

                setTimeout(function() {
                    mode_select.enable();
                }, 1500);

            });
        });


        /*
        const test_button = webix.$$('dvt:btn:test');

        test_button.attachEvent('onItemClick', function(){
            _this.command('set_mode', {
                base_mode: 1
                ,custom_mode: 0
            });
        });
        */




        //console.log(button_takeoff);

        //
        //  Отобразить данные на экране
        //

        // Маркер на карте
        _this.set_marker_map();

        // Привязка списка статусов
        statuses_list.data.sync(this.statuses_collection);

        // Привязка шаблона с данными телеметрии
        //telem_view.bind(this.telem2);

        // Привязка шаблона информации
        popup_info_tpl.bind(this.info);

        //telem_tpl.bind(this.telem_10hz);


        //
        //  Обработка событий
        //

        //
        // Кнопка Взлет

        /*
        button_takeoff.attachEvent('onItemClick', () => {
            // Запуск таймера отсчета времени исполнения команды
            let start = (new Date()).getTime();

            // Блокируем кнопку
            button_takeoff.disable();

            // Оформить обработчик ответа на команду
            // 22 = MAV_CMD -> MAV_CMD_NAV_TAKEOFF
            this.command_ack.wait(22, function(result){ // MAV_RESULT
                // После прихода ответа

                // Разблокируем кнопку
                button_takeoff.enable();

                // Показываем сообщение
                webix.message({
                    text: 'TAKEOFF: ' + result + ' (' + ((new Date()).getTime() - start) + 'ms)'
                    ,type: 'debug'
                    ,expire: 2000
                });
            });

            // Отправить команду
            this.command('takeoff', {alt: 30});

        });
        */


        console.log(relay1_switch);

        relay1_switch.attachEvent('onChange', (value, old_value) => {

            console.log('relay 1 switch');

            relay1_switch.disable();

            _this.command('switch_relay', {relay: 1, switch: value ? 'on' : 'off'}).then(function(res){
                if( 'success' === res ){
                    relay1_switch.enable();
                    webix.message('Реле 1 включено');
                }
            }).catch(function(err){
                relay1_switch.blockEvent();
                relay1_switch.setValue(old_value);
                relay1_switch.unblockEvent();
                relay1_switch.enable();
                webix.message({text: 'Реле 1 не ' + (value ? 'включилось' : 'выключилось'), type: 'error', expire: 1500});
            });

        });

        relay2_switch.attachEvent('onChange', (value, old_value) => {

            relay2_switch.disable();

            _this.command('switch_relay', {relay: 2, switch: value ? 'on' : 'off'}).then(function(res){
                if( 'success' === res ){
                    relay2_switch.enable();
                    webix.message('Реле 2 включено');
                }
            }).catch(function(err){
                relay2_switch.blockEvent();
                relay2_switch.setValue(old_value);
                relay2_switch.unblockEvent();
                relay2_switch.enable();
                webix.message({text: 'Реле 2 не ' + (value ? 'включилось' : 'выключилось'), type: 'error', expire: 1500});
            });

        });

        relay3_switch.attachEvent('onChange', (value, old_value) => {

            relay3_switch.disable();

            _this.command('switch_relay', {relay: 3, switch: value ? 'on' : 'off'}).then(function(res){
                if( 'success' === res ){
                    relay3_switch.enable();
                    webix.message('Реле 3 включено');
                }
            }).catch(function(err){
                relay3_switch.blockEvent();
                relay3_switch.setValue(old_value);
                relay3_switch.unblockEvent();
                relay3_switch.enable();
                webix.message({text: 'Реле 3 не ' + (value ? 'включилось' : 'выключилось'), type: 'error', expire: 1500});
            });

        });

        relay4_switch.attachEvent('onChange', (value, old_value) => {

            relay4_switch.disable();

            _this.command('switch_relay', {relay: 4, switch: value ? 'on' : 'off'}).then(function(res){
                if( 'success' === res ){
                    relay4_switch.enable();
                    webix.message('Реле 4 включено');
                }
            }).catch(function(err){
                relay4_switch.blockEvent();
                relay4_switch.setValue(old_value);
                relay4_switch.unblockEvent();
                relay4_switch.enable();
                webix.message({text: 'Реле 4 не ' + (value ? 'включилось' : 'выключилось'), type: 'error', expire: 1500});
            });

        });


        let video_timeout = null;

        //
        // Надо подождать пока загрузится DOM и включить плеер с видеотрансляцией
        setTimeout(function(){
            const el = document.getElementById('video_player');
            _this.player = new FlussonicMsePlayer(el, video_test_url, {
                debug: true
                ,onProgress: function(currentTime) {

                    if( video_timeout ) clearTimeout(video_timeout);

                    if( !video_player_tpl.isVisible() ){
                        video_player_tpl.show();
                    }

                    video_timeout = setTimeout(function(){
                        video_noise_tpl.show();
                    }, 3000);
                }

                ,onMediaInfo: function(rawMetaData) {
                    console.log('player onMediaInfo');
                    console.log(rawMetaData);
                }
            }); // WSS или WS

            /*
            _this.player.onProgress = function(currentTime) {

                if( video_timeout ) clearTimeout(video_timeout);

                if( !video_player_tpl.isVisible() ){
                    video_player_tpl.show();
                }

                video_timeout = setTimeout(function(){
                    video_noise_tpl.show();
                }, 3000);
            };

            _this.player.onMediaInfo = function(rawMetaData) {
                console.log('player onMediaInfo');
                console.log(rawMetaData);
            };
            */

            video_noise_tpl.show();

            if( _this.player ){
                _this.player.play();
                //video_player_tpl.show();
                //video_noise_tpl.show();
            }

            window.player = _this.player;

        }, 500);

        console.log('view_start 100');

    }

    view_stop(){
        // Остановить передачу телеметрии 2го уровня
        this.command();


        this.marker.setMap(null);

        if( player ) player = null;

        if( this.view ){
            try{
                //this.view.$scope.$$('data:telem').unbind();
                this.view.$scope.info_popup.queryView({localId: 'tpl:info'}).unbind();
                this.view = null;
            }
            catch(err){
                console.log(err);
            }
        }

    }

}

export default Drone;
