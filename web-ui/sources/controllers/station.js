//import timeformat from "../utils/timeformat";


const roof_status_opts = ['!-!-!', 'закрыта', 'открывается', 'открыта', 'закрывается'];
const dock_status_opts = ['!-!-!', 'закрыт', 'открывается', 'открыт', 'закрывается', 'контакт', 'перегруз', 'зарядка'];
const charge_status_opts = ['!-!-!', 'выключена', 'включена'];


let roof_status = null;
let dock_status = null;
let charge_status = null;

export default {

    init: function(view, url){

    }

    ,ready: function(view, url){
        const socket_io = window.socket_io;

        const roof_status_view = view.$scope.$$('tpl:roof_status');
        const roof_open = view.$scope.$$('btn:roof_open');
        const roof_close = view.$scope.$$('btn:roof_close');

        const dock_status_view = view.$scope.$$('tpl:dock_status');
        const dock_open = view.$scope.$$('btn:dock_open');
        const dock_close = view.$scope.$$('btn:dock_close');

        const charge_status_view = view.$scope.$$('tpl:charge_status');
        const charge_on = view.$scope.$$('btn:charge_on');
        const charge_off = view.$scope.$$('btn:charge_off');
        const charge_data = view.$scope.$$('tpl:charge_data');

        const set_roof_status = function(status){
            if( roof_status === status ) return;

            roof_status_view.setValues({roof_status: roof_status_opts[status]});
            roof_status = status;

            if( 0 === status ){
                roof_open.hide();
                roof_close.show();
                roof_close.enable();
            }
            else if( 1 === status ){
                roof_close.hide();
                roof_open.show();
                roof_open.enable();
            }
            else if( 2 === status ){
                roof_open.hide();
                roof_close.show();
                roof_close.disable();
            }
            else if( 3 === status ){
                roof_open.hide();
                roof_close.show();
                roof_close.enable();
            }
            else if( 4 === status ){
                roof_close.hide();
                roof_open.show();
                roof_open.disable();
            }
        };

        const set_dock_status = function(status){
            // 0 = ошибка, 1 = закрыт, 2 = открывается, 3 = открыт,
            // 4 = закрывается, 5 = контакт с дроном, 6 = перегруз по току на моторах)
            // 7 = идет зарядка
            if( dock_status === status ) return;

            dock_status_view.setValues({dock_status: dock_status_opts[status]});
            dock_status = status;

            if( 0 === status || 1 === status || 5 === status || 6 === status){
                dock_close.hide();
                dock_open.show();
                dock_open.enable();
            }
            else if( 2 === status ){
                dock_close.show();
                dock_close.disable();
                dock_open.hide();
            }
            else if( 3 === status ){
                dock_close.show();
                dock_close.enable();
                dock_open.hide();
            }
            else if( 4 === status || 7 === status){
                dock_close.hide();
                dock_open.show();
                dock_open.disable();
            }
        };

        const set_charge_status = function(status){
            if( charge_status === status ) return;

            charge_status_view.setValues({charge_status: charge_status_opts[status]});
            charge_status = status;

            if( 1 === status ){
                charge_off.hide();
                charge_on.show();
                charge_data.hide();
                charge_data.setValues(charge_data.config.data);
            }
            else if( 2 === status ){
                charge_on.hide();
                charge_off.show();
                charge_data.show();
            }
            else {
                charge_on.hide();
                charge_off.hide();
            }
        };

        set_roof_status(0);
        set_dock_status(0);
        set_charge_status(0);

        socket_io.on('station_status', function (data) {
            //console.log(data);
            set_roof_status(data.r1);
            set_dock_status(data.d1);
            set_charge_status(data.c1);
        });

        socket_io.on('charge', function (data) {
            charge_data.setValues(data);
        });

        roof_open.attachEvent('onItemClick', function(){
            socket_io.emit('station_command', 'roof_open');
        });

        roof_close.attachEvent('onItemClick', function(){
            socket_io.emit('station_command', 'roof_close');
        });


        dock_open.attachEvent('onItemClick', function(){
            socket_io.emit('station_command', 'dock_open');
        });

        dock_close.attachEvent('onItemClick', function(){
            socket_io.emit('station_command', 'dock_close');
        });


        charge_on.attachEvent('onItemClick', function(){
            socket_io.emit('station_command', 'charge_on');
        });

        charge_off.attachEvent('onItemClick', function(){
            socket_io.emit('station_command', 'charge_off');
        });

    }

    ,destroy: function(){

        const socket_io = window.socket_io;

        socket_io.off('station_status');
        socket_io.off('charge');

    }

};
