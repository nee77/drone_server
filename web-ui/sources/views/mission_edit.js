import {JetView} from "webix-jet";
//import controllers from './../controllers/mission_edit';


export default class MissionEditView extends JetView{
    config(){
        return view_config;
    }

    init(view, url){

        this.address_popup = this.ui(address_popup);
        this.point_popup = this.ui(point_popup);

        this.$$('mission:map').getMap().setOptions(map_config);

        //controllers.init(view, url);

    }

    ready(view, url){
        //controllers.ready(view, url);
    }

    urlChange(view, url) {}

    destroy(){
        //controllers.destroy(this);
    }
}


// Форма параметров полетного задания
const mission_form = {
    view: 'form'
    ,borderless: true
    ,localId: 'mission:form'
    ,elementsConfig:{
        labelWidth: 150
    }
    ,rows: [
        { view: 'template', borderless: true, height: 30, localId: 'tpl:location', template: 'Местоположение: #loc#', data: {loc: ''}}
        ,{ view: 'text', name: 'name', label: 'Название задания' }
        ,{ view: 'richselect', name: 'drone_id', localId: 'select:uav', label: 'БПЛА', options: []}
    ]
};


// Форма параметров точки
const waypoint_form = {
    view:"form"
    ,borderless: true
    ,elementsConfig:{
        labelWidth: 160
    }
    ,elements:[
        {
            view: 'multiview'
            ,fitBiggest: true
            ,animate: false
            ,cells: [
                {
                    template: ''
                }
                ,{
                    height: 70
                    ,rows: [
                        { view: 'counter', name: 'takeoff_alt', label: 'Высота взлета, м', value: 0, title: 'На какую высоту взлететь прежде, чем полететь к первой точке', labelWidth: 150 }
                        ,{ view: 'checkbox', name: 'rtl_end', label: "Возврат в точку старта по окончанию", value:1, labelWidth: 270 }
                    ]
                }
                ,{
                    height: 200
                    ,rows: [
                        { view: 'counter', name: 'alt', label: 'Высота, м', value: 0, title: 'Высота пролета точки относительно точки старта или поверхности земли' }
                        ,{ view: 'radio', name: 'alt_rel', value: 1, label: 'Относительно', options: [{id: 'home', value: "старта"}, {id: 'ground', value: "земли"}], vertical: false }
                        ,{ view: 'counter', name: 'speed', label: 'Скорость, км/ч', value: 0, title: 'Скорость в этой точке', min: 0, max: 1000 }
                        ,{ view: 'counter', name: 'hold', label: 'Зависнуть на, сек', value: 0, title: 'На какое время зависнуть в точке прежде, чем полететь к следующей (СЕКУНДЫ)', min: 0, max: 1000 }
                        ,{
                            cols: [
                                {gravity: 3}
                                ,{ view: 'icon', icon: 'trash', localId: 'button:remove_point', tooltip: 'Удалить точку'}
                            ]
                        }

                        /*
                        ,{
                            cols: [
                                {
                                    view:"richselect"
                                    ,width: 230
                                    ,label: 'Camera'
                                    ,name: 'cam'
                                    ,value:1, options:[
                                         { id: 1, value: 'no change' }
                                        ,{ id: 2, value: 'take photo' }
                                        ,{ id: 3, value: 'take photos (time)' }
                                        ,{ id: 4, value: 'take photos (distance)' }
                                        ,{ id: 5, value: 'stop taking photos' }
                                        ,{ id: 6, value: 'start video' }
                                        ,{ id: 7, value: 'stop video' }
                                    ]
                                }
                                ,{ view: 'text', name: 'cam_dist', label: 'Distance' }
                            ]
                        }
                        */
                    ]
                }
            ]
        }

    ]

};


// Боковая панель редактирования задания
const edit_side_panel = {
    borderless: true
    ,rows: [

        // top toolbar with controls
        {
            view: 'toolbar'
            ,type: 'clean'
            //,height: 60
            ,elements: [
                {view:'icon', localId: 'button:return', icon: 'chevron-left', tooltip: 'Вернуться к списку'}
                ,{}
                ,{ view: 'icon', localId: 'button:trash', icon: 'trash', tooltip: 'Удалить это полетное задание', width: 60 }
            ]
        }

        ,mission_form

        // Lock button
        ,{
            cols: [
                {
                    view: "toggle",
                    type: "iconButton",
                    localId: "button:add_point",
                    icon: "circle",
                    label: "Точка",
                    autowidth: true
                    ,tooltip: 'Добавление новых точек в маршрут'
                    ,value: 0
                }
                ,{
                    view: "toggle",
                    type: "iconButton",
                    localId: "button:add_point",
                    icon: "plus-square",
                    label: "Площадка",
                    autowidth: true
                    ,tooltip: 'Добавление площадки облета в маршрут'
                    ,disabled: true
                }
            ]
        }

        // Waypoints table
        ,{
			view: "datatable"
            ,localId: 'table:points'
			,select: true
            ,borderless: true
			,data: []//list_data
            ,columns:[
					{ id:"title", header:"Точка", template:"<b>#seq#</b> #title#", fillspace: true},
					{ id:"alt",	header:"М", width: 100},
					{ id:"spd",	header:"КМ/Ч", width: 100}
				]
            //,autowidth: true
			,subview: waypoint_form

		}


    ]

};


// Все окно интерфейса
const view_config = {
    padding: 0
    ,borderless: true
    ,border: false
	,cols: [
        {
            rows: [
                // карта
                {
                    view: "google-map",
                    localId: "mission:map",
                    zoom:13,
                    mapType: 'SATELLITE',
                    gravity: 3,
                    center:[ 55.751244, 37.618423 ]
                }
                ,{
                    height: 100
                    ,cols: [
                        // Вычисляемые данные о маршруте
                        {
                            width: 200
                            ,localId: 'tpl:route_data'
                            ,template: ''
                                + 'Длина маршрута: #dist# км<br/>'
                                + 'Время полета: X мин<br/>'
                                //+ 'Макс высота: 120 м'
                        }

                        // график высоты
                        ,{
                            view: "chart"
                            ,padding: 20
                            ,height: 100
                            ,localId: "chart:alt"
                            ,type:"line"
                            ,value: "#alt#"
                            ,tooltip:{
                                template: "Высота: #alt# м, скорость: #spd# км/ч"
                            }
                            ,item:{
                                borderColor: "#1293f8",
                                color: "#ffffff"
                            }
                            ,line:{
                                color:"#1293f8",
                                width:3
                            }
                            ,xAxis:{
                                template: "#seq#"
                                ,lines: false
                            }
                            ,offset:0
                            ,yAxis:{
                                start:0
                                //end:100,
                                ,step:10
                                ,lines: false
                                ,template:function(obj){
                                    return (obj%20?"":obj)
                                }
                            }
                            //,tooltip: 'Высота точек маршрута'
                        }
                    ]
                }

            ]
        }

        // боковая панель редактирования
		,{
            width: 500
			,gravity: 2
            ,borderless: true
            ,body: {
                borderless: true
                ,rows: [
                    edit_side_panel
                ]
            }
        }

	]
};


// Всплывающее окно поиска адреса
const address_popup = {
    view:"window",
    //id:"new_map_popup",
    //height: 250,
    width: 300,
    top: 100,
    left: 100
    ,headHeight: 1
    ,padding: 5
    ,body:{
        borderless: true
        ,padding: 5
        ,rows: [
            {
                template: 'Введите адрес или координаты<br/>для поиска'
                ,height: 70
                ,borderless: true
            }
            ,{
                view: 'text'
                ,localId: 'text:search_loc'
                ,labelWidth: 10
                ,placeholder: 'город, адрес или координаты'
            }
            ,{
                cols: [
                    {
                        view: 'button'
                        ,localId: 'button:search_loc'
                        ,label: 'Найти'
                        //,width: 80
                        ,css: 'button_primary button_raised'
                    }
                    ,{width: 20}
                    ,{
                        view: 'button'
                        ,label: 'Закрыть'
                        //,width: 80
                        ,css: 'button_primary'
                        ,click: function(){
                            this.getTopParentView().hide();
                        }
                    }
                ]
            }
        ]
    }
};


// Всплывающее окно параметров точки маршрута
const point_popup = {
    view:"popup",
    //id:"new_map_popup",
    //height: 250,
    width: 300,
    top: 200,
    left: 200
    ,headHeight: 1
    ,padding: 5
    ,body:{
        borderless: true
        ,padding: 5
        ,rows: [
            {
                template: 'Редактирование параметров точки'
                ,height: 70
                ,borderless: true
            }
        ]
    }
};


const map_config = {
    fullscreenControl: false
    ,panControl: false
    ,rotateControl: false
    ,streetViewControl: false
    ,scaleControl: false
    ,mapTypeControlOptions: {
        position: google.maps.ControlPosition.TOP_RIGHT
    }
    ,zoomControlOptions: {
        position: google.maps.ControlPosition.RIGHT_TOP
    }
};

