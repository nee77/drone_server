import {JetView} from "webix-jet";
import io from 'socket.io-client';
import top_toolbar	from "views/toolbars/top_toolbar";
import sidebar from "views/menus/sidebar";

window.socket_io = null;


export default class AppView extends JetView {
	config(){
		return layout;
	}

	init(view, url){
        // после инициализации главного вида нужно соединиться с socket.io
        const loginService = this.app.getService('user');
        const user = loginService.getUser();


        window.socket_io = io({query: {me:'gcs', user: user.user, io_key: user.io_key}});

        const socket_io = window.socket_io;

        // События соединения socket.io
        socket_io.on('connect', function(){
            console.log('socket.io CONNECTED');
        });
        socket_io.on('disconnect', function(){
            console.log('socket.io DISCONNECTED');
        });
        socket_io.on('status', function(status){
            if( 'unauthorized' === status ){
                loginService.logout();
                view.$scope.app.show('/app/login');
            }
        });


	}
}


const layout = {
	type: 'material'
    ,borderless: true
    ,padding: 0
    ,rows:[

		top_toolbar

        ,{
		    cols: [

			    sidebar

                ,{ $subview: true }

			]
		}
	]
};


