import {JetView} from "webix-jet/dist/index";


//import controllers from './../controllers/missions';


export default class MissionsView extends JetView {
    config(){
        return view_config;
    }

    init(view, url){
        //controllers.init(view);
    }

    ready(view, url){
        //controllers.ready(view);
    }

    urlChange(view, url){}

    destroy(){}

}

//
// Основная таблица со списком
const view_config = {
    type: 'clean'
    ,rows: [

        // Верхняя панель с кнопками
        {
            view: 'toolbar'
            ,type: 'clean'
            ,elements: [
                // Кнопка Добавить нового дрона
                {
                    view: 'button'
                    ,type: 'iconButton'
                    ,localId: 'button:add'
                    ,label: 'Новое задание'
                    ,icon: 'plus'
                    ,css: 'button_primary'
                    ,autowidth: true
                }
                ,{}
            ]
        }

        // Таблица
        ,{
            view:"datatable"
            ,localId: 'table:missions'
            ,select: true
            ,columns:[
                { id: "name",	header:"Название", width: 200},
                { id: "location",	header:"Местоположение", fillspace: true},
                { id: "uav",	header:"БПЛА", 	width:100},
                { id: "dist",	header:"Длина" , width:100},
                { id: "time",	header:"Время", 	width:100},
                 { id: "date",	header:"Дата" , width:100},
                 { id: "status",	header:"Статус" , width:100},
            ]
        }
    ]

};



