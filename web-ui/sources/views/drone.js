import {JetView} from "webix-jet";

import controllers from '../controllers/drone_view';


let view_controls_id = null;


export default class DroneView extends JetView {
    config(){
        //this.keys_window = this.ui(keys_window);
        return view_config;
    }

    init(view, url){
        this.action_menu = this.ui(action_menu_popup);
        this.info_popup = this.ui(info_popup);
        this.statuses_popup = this.ui(statuses_popup);
        this.fi_popup = this.ui(fi_popup);

        view_controls_id = webix.$$('view_controls').addView(view_controls);

        controllers.init(view, url);
    }

    ready(view, url){

        const map = this.$$('map:drone');
        const fi_popup = this.fi_popup;
        const top_toolbar = webix.$$('top_toolbar');

        //console.log('1: ' + map.$height);

        //setTimeout(() => {
        //  console.log('3: ' + map.$height);
        //}, 1000);

        this.$$('map:drone').getMap(true).then(function(mapObj) {
            mapObj.setOptions(map_options);

            fi_popup.show({y:70, x: (top_toolbar.$width - fi_popup.$width - 20)});

            //console.log('2: ' + map.$height);

            controllers.ready(view, url);
        });


    }

    destroy(){
        if( webix.$$('view_controls') && view_controls_id ){
            webix.$$('view_controls').removeView(view_controls_id);
        }

        controllers.destroy();
    }
}




const action_menu_popup = {
    view: 'popup'
    ,id: 'drone_view_popup_action_menu'
    ,body: {
        width:300
        ,rows: [

            {
                view: 'button'
                ,type: 'iconButton'
                ,localId: 'btn:takeoff'
                ,label: 'Взлет'
                ,icon: 'upload'
            }

            ,{
                view: 'button'
                ,type: 'iconButton'
                ,id: 'btn:mission'
                ,label: 'Круг'
                ,icon: 'sync'
            }

            ,{
                view: 'button'
                ,type: 'iconButton'
                ,id: 'btn:land'
                ,label: 'Посадка'
                ,icon: 'download'
            }

            ,{ view: "switch", value: 0, label: "Реле 1",localId: 'sw:rel1' }
            ,{ view: "switch", value: 0, label: "Реле 2",localId: 'sw:rel2' }
            ,{ view: "switch", value: 0, label: "Реле 3",localId: 'sw:rel3' }
            ,{ view: "switch", value: 0, label: "Реле 4",localId: 'sw:rel4' }

            ,{}
        ]

    }
};


const info_popup = {
    view: 'popup'
    ,id: 'drone_view_popup_info'
    ,body: {
        width:300
        ,borderless: true
        ,rows: [
            {
                template: '#autopilot_str#, #type_str#, mavlink v.#mav_v# <br/> Включен: #time_on#'
                ,localId: 'tpl:info'
                ,height: 50
                ,borderless: true
            }
        ]

    }
};


const statuses_popup = {
    view: 'popup'
    ,id: 'drone_view_popup_statuses'
    ,body: {
        width: 300
        ,height: 500
        ,borderless: true
        ,rows: [
            {
                view: 'list'
                ,localId: 'list:statuses'
                ,template: '#text#'
            }
        ]

    }
};





/*
                            'GPS speed: #gps_speed# м/с<br/>' +
                            'Coords: #lat#, #lon# <br/>' +
                            'GPS alt: #alt# м<br/>' +
                            'Sats num: #sats# <br/>' +
                            'GPS Fix: #gps_fix_type# <br/>' +
                            'Sys load: #sys_load# %<br/>' +
                            'Uptime: #uptime_formatted# <br/>'
                            + '<br/>'
                            + 'Battery: #bat_v# V<br/>'
                            + 'Air pressure: #press_a# гПа<br/>'
                            + 'Air temp: #temp#<sup>o</sup>C <br/>'
                            + 'Roll: #roll#<sup>o</sup>, Pitch: #pitch#<sup>o</sup>, Compass: #yaw#<sup>o</sup> <br/>'
                            + '<br/>'
                            + 'PLat: #pos_lat#, PLon: #pos_lon#<br/>'
                            + 'PAlt: #pos_alt#, PRelAlt: #pos_rel_alt#<br/>'
                            + 'VX: #pos_vx#, VY: #pos_vy#, VZ: #pos_vz#<br/>'
                            + 'PHDG: #pos_hdg#<br/>'
                            // + 'Бортовое время: #time_formatted# <br/>'
                            */



//
// Кнопки для верхней панели
const view_controls = {
    cols: [
        // Кнопка с информацией
        { view: 'icon', icon: 'information', popup: 'drone_view_popup_info' }
        // Кнопка со списком статусов
        ,{ view: 'icon', icon: 'bullhorn', popup: 'drone_view_popup_statuses' }
        ,{}

        //,{ template: 'Time: #time_u#,   #time_b#,  #last_timestamp#' ,id: 'dvt:tpl:time' }

        //,{ view: 'button' ,value: 'TEST' ,css: 'button_primary' ,id: 'dvt:btn:test' }

        ,{
            view: 'richselect'
            ,id: 'dvt:rs:mode'
            ,label: 'Mode'
            ,labelWidth: 60
            ,width: 230
            ,options: []
        }

        //,{view: 'label', label: 'init', width: 130, id: 'dvt:lbl:mode' }
        //,{width: 10}
        ,{view: 'label', label: 'init', width: 80, id: 'dvt:lbl:armed' }
        ,{view: 'button', value: 'ARM', id: 'dvt:btn:arm', type: 'danger', width: 70, hidden: true }
        ,{view: 'button', value: 'DISARM', id: 'dvt:btn:disarm', type: 'danger', width: 70, hidden: true}


        ,{width: 10}
        // Кнопка меню команд
        ,{view: 'icon', icon: 'gamepad', popup: 'drone_view_popup_action_menu'}
    ]
};


// Параметры карты
const map_options = {
    fullscreenControl: false
    ,panControl: false
    ,rotateControl: false
    ,streetViewControl: false
    ,scaleControl: false
    ,zoomControlOptions: {
        position: google.maps.ControlPosition.LEFT_TOP
    }
};

const map_config = {
    view:"google-map",
    localId: "map:drone",
    zoom:13,
    mapType: 'SATELLITE',
    center:[ 55, 37 ]
};


// Видео и полетные индикаторы
const fi_popup = {
    view: 'window'
    ,id: 'drone_view_popup_fi'
    //,top: 50
    //,left: 50
    ,css: 'transp'
    ,head: false
    ,borderless: true
    ,body: {
        width:340
        ,borderless: true
        ,css: 'transp'
        ,rows: [

            // video screen
            {
                view: 'multiview'
                ,width: 340
                ,height: 260
                ,animate: false
                ,css: 'transp_9'
                ,cells: [
                    {
                        template: '<div style="text-align: center"><video id="video_player" style="width: 320px; height: 240px; align-self: center"/></div>'
                        //template: '<iframe style="width:320px; height:240px;" src="http://video.roboflot.ru:8080/test11011/drone1/embed.html?realtime=true&debug=false"></iframe>'
                        ,height: 300
                        ,localId: 'tpl:video_player'
                    }
                    ,{
                        template: '<img src="static/white_noise.gif" class="video_noise_320">'
                        ,height: 300
                        ,localId: 'tpl:video_noise'
                    }
                ]
            }

            ,{
                height: 170
                ,css: 'transp_all'
                ,borderless: true
                ,cols: [

                    // Горизонт
                    {
                        view: 'fi_horizon'
                        ,localId: 'fi:horizon'
                        ,size: 160
                        ,css: 'transp'
                        ,width: 170
                        ,borderless: true
                    }

                    // Компас
                    ,{
                        view: 'fi_compass'
                        ,localId: 'fi:compass'
                        ,size: 160
                        ,css: 'transp'
                        ,width: 170
                        ,borderless: true
                    }
                ]
            }


            ,{
                template: 'Roll #r#, pitch #p#, yaw #y#<br/>' +
                    'Спутников: #sats#<br/>' +
                    'Высота: #alt#<br/>' +
                    'Скорость: #speed#<br/>' +
                    'Sys load: #sys_load# %<br/><br/>'
                //,css: 'transp_9'
                ,height: 150
                ,localId: 'tpl:telem'
                ,data: {r: '', p: '', y: '', sats: '', alt: '', speed: '', sys_load: ''}
            }

        ]

    }
};


const view_config = {
    padding: 0
    ,borderless: true
    ,border: false
    ,localId: 'body'
	,cols: [
		// map
		map_config

	]
};




