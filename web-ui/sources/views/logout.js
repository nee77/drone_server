import {JetView} from "webix-jet";

export default class LogoutView extends JetView {
    config() {
        return {
            type: "line"
        }
    }

    init(view, url){
        this.app.getService('user').logout();
        this.app.show('/app/login');
    }

    ready(view, url){

    }

}
