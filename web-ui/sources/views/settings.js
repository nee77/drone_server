import {JetView} from "webix-jet";

//import controllers from '../controllers/profile';


export default class SettingsView extends JetView {
    config(){
        return view_config;
    }

    init(view, url){
        //controllers.init(view, url);
    }

    ready(view, url){
        //controllers.ready(view, url);
    }

    urlChange(view, url){}

    destroy(){
        //controllers.destroy(this);
    }

}



//
// Основной вид
const view_config = {
    template: 'settings'
};

