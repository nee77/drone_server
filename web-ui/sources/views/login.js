import {JetView} from "webix-jet";
import * as Cookies from "js-cookie";

export default class LoginView extends JetView{
	config(){

	    // Если данные сохранены в cookie, взять их оттуда
	    let values = webix.copy({
            user: '',
            pass: ''
        }, Cookies.getJSON("signin"));

        // Форма логина
        const login_form = {
            view:"form"
            ,width: 400
            ,borderless: true
            ,padding: 30
            ,rows:[
                { view:"text", name:"user", label:"Логин", labelPosition: "top", placeholder: 'user', value: values.user},
                { view:"text", type:"password", name:"pass", label: "Пароль", placeholder: '*******', labelPosition:"top", value: values.pass }

                ,{ view:"button", value: "Войти", css: 'button_primary button_raised', click: function() {
                    this.$scope.do_login();
                }, hotkey:"enter" }
            ],
            rules: {
                user: webix.rules.isNotEmpty,
                pass: webix.rules.isNotEmpty
            }
        };

        return { rows: [ {}, { cols:[ {}, login_form, {}]}, {} ] };

	}

	init(view, url){
		// view.$view.querySelector("input").focus();
	}

	do_login(){
	    const loginService = this.app.getService('user');

		//const user = this.app.getService("user");
		const form = this.getRoot().queryView({ view:"form" });

		if (form.validate()){
			const values = form.getValues();

            Cookies.set("signin", values);

            form.disable();

            loginService.login(values.user, values.pass).catch( e => {
                webix.message({
                    type: "error",
                    text: "Неверный логин или пароль"
                });

                form.enable();
                form.focus();
            });

		} else {
		    webix.message('Введите логин и пароль');
        }

	}

}


