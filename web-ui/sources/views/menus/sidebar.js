import {JetView, plugins} from "webix-jet";

// Боковое меню
const menu_data = [
     { id: "drone", icon: "drone", value: "Дрон" }
    ,{ id: "missions", icon: "map-marker-distance", value: "Полетные задания" }
    ,{ id: "station", icon: "package-variant", value: "Станция" }
    ,{ id: "settings", icon: "settings", value:"Настройки" }
];


// Инициализация
export default class SidebarView extends JetView{
	config(){
		return {
		    id: "sidebar1"
			,view: "sidebar"
			,collapsed: true
			,select: true
			,on:{
				onBeforeSelect:function(id){
				    if(this.getItem(id).$count){
						return false;
					}
				},
				onAfterSelect:function(id){
					const item = this.getItem(id);
					webix.$$('app_head_title').setValues(item.value);
				}
			}
		};
	}

    init(view){
		webix.$$("sidebar1").parse(menu_data);
		this.use(plugins.Menu, "sidebar1");
	}
}

