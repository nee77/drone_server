import {JetView} from "webix-jet";

import controllers from './../controllers/station';


export default class StationView extends JetView {
    config(){
        return view_config;
    }

    init(view, url){
        controllers.init(view, url);
    }

    ready(view, url){
        controllers.ready(view, url);
    }

    destroy(view, url){
        controllers.destroy(view, url);
    }
}


const data_view = {
    view: 'scrollview'
    ,body: {
        template: ''
            + 'АЦП 0: #adc0# <br/>'
            + 'АЦП 1: #adc1# <br/>'
            + 'АЦП 2: #adc2# <br/>'
            + 'АЦП 3: #adc3# <br/>'
            + 'АЦП 4: #adc4# <br/>'
            + 'АЦП 5: #adc5# <br/>'
            + 'АЦП 6: #adc6# <br/>'
            + 'АЦП 7: #adc7# <br/>'

    }
};


//
const view_config = {
    padding: 0
    , minWidth: 800
    , minHeight: 600
    , borderless: true
    , rows: [
        {
            cols: [

                // Камера
                {
                    template: 'camera'
                    ,gravity: 2
                }

                // Боковая панель
                , {
                    width: 350
                    ,borderless: true
                    ,rows: [
                        {
                            rows: [
                                {
                                    height: 20
                                }

                                // Состояние и управление крышей
                                ,{
                                    cols: [
                                        { template: 'Крыша: #roof_status#', localId: 'tpl:roof_status', data: {roof_status: ''}, width: 200, borderless: true }
                                        ,{
                                            rows: [
                                                { view: 'button', label: 'Открыть', localId: 'btn:roof_open', hidden: true, disabled: true, css: 'button_primary button_raised' }
                                                ,{ view: 'button', label: 'Закрыть', localId: 'btn:roof_close', hidden: true, disabled: true, css: 'button_primary button_raised' }
                                            ]
                                            ,borderless: true
                                        }
                                    ]
                                    ,height: 50
                                    ,borderless: true
                                }

                                // Состояние и управление доком
                                ,{
                                    cols: [
                                        { template: 'Док: #dock_status#', localId: 'tpl:dock_status', data: {dock_status: ''}, width: 200, borderless: true }
                                        ,{
                                            rows: [
                                                { view: 'button', label: 'Открыть', localId: 'btn:dock_open', hidden: true, css: 'button_primary button_raised' }
                                                ,{ view: 'button', label: 'Закрыть', localId: 'btn:dock_close', hidden: true, css: 'button_primary button_raised' }
                                            ]
                                            ,borderless: true
                                        }
                                    ]
                                    ,height: 50
                                    ,borderless: true
                                }

                                // Состояние и управление зарядкой
                                ,{
                                    cols: [
                                        { template: 'ЗУ: #charge_status#', localId: 'tpl:charge_status', data: {charge_status: ''}, width: 200, borderless: true }
                                        ,{
                                            rows: [
                                                { view: 'button', label: 'Включить', localId: 'btn:charge_on', hidden: false, css: 'button_danger button_raised' }
                                                ,{ view: 'button', label: 'Выключить', localId: 'btn:charge_off', hidden: true, css: 'button_primary button_raised' }
                                            ]
                                            ,borderless: true
                                        }
                                    ]
                                    ,height: 50
                                    ,borderless: true
                                }
                                ,{
                                    localId: 'tpl:charge_data'
                                    ,template: 'Время зарядки: #t#, #wh# Ватт*ч<br/>#v# Вольт, #i# А'
                                    ,data: {t:0, wh: 0, v: 0, i: 0}
                                    ,hidden: true
                                }

                                // Время и параметры зарядки


                            ]
                        }

                        //,data_view

                        ,{

                        }

                    ]
                }

            ]
        }

    ]
};