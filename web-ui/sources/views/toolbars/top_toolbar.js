import {JetView} from "webix-jet";

import controllers from './../../controllers/top_toolbar';


export default class TopToolbarView extends JetView {
    config(){
        return view_config;
    }

    init(view, url){
        controllers.init(view, url);
    }

    ready(view, url){
        controllers.ready(view, url);
    }

    destroy(view, url){
        //controllers.destroy(view, url);
    }
}



const view_config = {
    view: "toolbar"
    ,padding: 5
    ,type: 'clean'
    ,css: 'bg_panel'
    ,id: 'top_toolbar'
    ,borderless: true
    ,elements: [
        {view: "icon", icon: "menu"
            ,click: function(){
                webix.$$("sidebar1").toggle();

                let icon = 'close';

                if( $$("sidebar1").config.collapsed ){
                    icon = 'menu';
                }

                this.define('icon', icon);
                this.refresh();
            }
        }
        ,{ view: 'template', id: 'app_head_title', width: 150, template: function(data){

            return data;

        }}

        //,{ template: 'Т 25С, ветер 3-6 м/с СВ', width: 200 }
        //,{ template: 'Время станции: #station_time#', localId: 'tpl:station_time', data: {station_time: '---'}, width: 200 }
        //,{ template: 'Время дрона: #drone_time#', localId: 'tpl:drone_time', data: {drone_time: '---'}, width: 200 }
        //,{ template: 'Связь с дроном:', width: 130 }
        //,{ view: "icon", icon: "wifi", tooltip: 'WiFi', disabled: true}
        //,{ view: "icon", icon: "wifi", tooltip: 'Modem'}
        //,{ view: "icon", icon: "wifi", tooltip: '4G'}

        ,{
            id: 'view_controls'
            ,cols: []
            ,gravity: 4
        }

    ]
};
