module.exports = {

    DRONE_ID: 'drone_id_1234567890'
    ,DRONE_SYSID: 1
    ,DRONE_COMPID: 1
    ,DRONE_MAVLINK_VERSION: 'v1.0'
    ,DRONE_MAVLINK_MSG_DEF: ["common", "ardupilotmega"]

    ,STATION_IO_KEY: 'station_key_87879832523'

    ,WEB_SERVER_PORT: 8091

    ,SESSION_SECRET: 'supersecretword48397593465872364857426387'

    ,REDIS_HOST: 'localhost'
    ,REDIS_PORT: 6379

    ,RETHINKDB_SERVER: 'localhost'
    ,RETHINKDB_PORT: 28015
    ,RETHINKDB_DB: 'drone_server'

};