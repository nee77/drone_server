const mav_json = function(mavlink, mav_data){

    mav_data['telem_1hz'] = {
        time: 0

        // 0 HEARTBEAT
        ,b_mode: null // base mode from heartbeat
        ,c_mode: null // custom mode from heartbeat
        ,sys_status: null // system status from heartbeat

        // 1 SYS_STATUS
        ,sys_load: 0
        ,bat_v: 0 // Battery voltage, in millivolts (1 = 1 millivolt) (Units: mV)
        ,bat_c: 0 // Battery current, in 10*milliamperes (1 = 10 milliampere), -1: autopilot does not measure the current (Units: cA)
        ,bat_rem: 0 // in % 0-100
        ,sens_present: 0
        ,sens_enabled: 0
        ,sens_health: 0

        // 2 SYSTEM_TIME
        ,time_u: 0
        ,time_b: 0

        // 24 GPS_RAW_INT
        ,gps_fix: 0
        ,lon: 0
        ,lat: 0
        ,alt: 0
        ,gps_speed: 0
        ,gps_cog: 0 // Course over ground (NOT heading, but direction of movement) in degrees * 100
        ,sats: 0

        // 29 SCALED_PRESSURE
        ,press_a: -1 // Absolute pressure (hectopascal) (Units: hPa)
        ,press_d: -1 // Differential pressure 1 (hectopascal) (Units: hPa)
        ,temp: -100 //

        // 33 GLOBAL_POSITION_INT
        ,pos_lat: 0
        ,pos_lon: 0
        ,pos_alt: 0
        ,pos_rel_alt: 0
        ,pos_vx: 0
        ,pos_vy: 0
        ,pos_vz: 0
        ,pos_hdg: 0

    };

    mav_data['telem_10hz'] = {
        // 30 ATTITUDE
         r: 0
        ,p: 0
        ,y: 0
        ,rs: 0
        ,ps: 0
        ,ys: 0
    };

    mav_data['info'] = {
        // 0 HEARTBEAT
        type: 0
        ,autopilot: 0
        ,mav_v: 0
    };


    // 0 HEARTBEAT
    mavlink.on("HEARTBEAT", (message, fields) => {
        /*
            type	uint8_t	Type of the MAV (quadrotor, helicopter, etc., up to 15 types, defined in MAV_TYPE ENUM) (Enum:MAV_TYPE )
            autopilot	uint8_t	Autopilot type / class. defined in MAV_AUTOPILOT ENUM (Enum:MAV_AUTOPILOT )
            base_mode	uint8_t	System mode bitfield, see MAV_MODE_FLAG ENUM in mavlink/include/mavlink_types.h (Enum:MAV_MODE_FLAG )
            custom_mode	uint32_t	A bitfield for use for autopilot-specific flags.
            system_status	uint8_t	System status flag, see MAV_STATE ENUM (Enum:MAV_STATE )
            mavlink_version	uint8_t_mavlink_version	MAVLink version, not writable by user, gets added by protocol because of magic data type: uint8_t_mavlink_version

            MAV_MODE_FLAG
                128	MAV_MODE_FLAG_SAFETY_ARMED	0b10000000 MAV safety set to armed. Motors are enabled / running / can start. Ready to fly. Additional note: this flag is to be ignore when sent in the command MAV_CMD_DO_SET_MODE and MAV_CMD_COMPONENT_ARM_DISARM shall be used instead. The flag can still be used to report the armed state.
                64	MAV_MODE_FLAG_MANUAL_INPUT_ENABLED	0b01000000 remote control input is enabled.
                32	MAV_MODE_FLAG_HIL_ENABLED	0b00100000 hardware in the loop simulation. All motors / actuators are blocked, but internal software is full operational.
                16	MAV_MODE_FLAG_STABILIZE_ENABLED	0b00010000 system stabilizes electronically its attitude (and optionally position). It needs however further control inputs to move around.
                8	MAV_MODE_FLAG_GUIDED_ENABLED	0b00001000 guided mode enabled, system flies waypoints / mission items.
                4	MAV_MODE_FLAG_AUTO_ENABLED	0b00000100 autonomous mode enabled, system finds its own goal positions. Guided flag can be set or not, depends on the actual implementation.
                2	MAV_MODE_FLAG_TEST_ENABLED	0b00000010 system has a test mode enabled. This flag is intended for temporary system tests and should not be used for stable implementations.
                1	MAV_MODE_FLAG_CUSTOM_MODE_ENABLED	0b00000001 Reserved for future use.

            MAV_STATE
                MAV_STATE_UNINIT	Uninitialized system, state is unknown.
                MAV_STATE_BOOT	System is booting up.
                MAV_STATE_CALIBRATING	System is calibrating and not flight-ready.
                MAV_STATE_STANDBY	System is grounded and on standby. It can be launched any time.
                MAV_STATE_ACTIVE	System is active and might be already airborne. Motors are engaged.
                MAV_STATE_CRITICAL	System is in a non-normal flight mode. It can however still navigate.
                MAV_STATE_EMERGENCY	System is in a non-normal flight mode. It lost control over parts or over the whole airframe. It is in mayday and going down.
                MAV_STATE_POWEROFF	System just initialized its power-down sequence, will shut down now.
                MAV_STATE_FLIGHT_TERMINATION	System is terminating itself.
         */

        mav_data['info'].type = fields.type;
        mav_data['info'].autopilot = fields.autopilot;
        mav_data['info'].mav_v = fields.mavlink_version;

        mav_data['telem_1hz'].b_mode = fields.base_mode;
        mav_data['telem_1hz'].c_mode = fields.custom_mode;
        mav_data['telem_1hz'].sys_status = fields.system_status;
    });

    // 1 SYS_STATUS
    mavlink.on("SYS_STATUS", function(message, fields) {
        mav_data['telem_1hz'].sys_load = Math.round(fields.load/10);
        mav_data['telem_1hz'].bat_v = fields.voltage_battery / 1000;
        mav_data['telem_1hz'].bat_c = fields.current_battery;
        mav_data['telem_1hz'].bat_rem = fields.battery_remaining;

        mav_data['telem_1hz'].sens_present = fields.onboard_control_sensors_present;
        mav_data['telem_1hz'].sens_enabled = fields.onboard_control_sensors_enabled;
        mav_data['telem_1hz'].sens_health = fields.onboard_control_sensors_health;

        /*
            onboard_control_sensors_present	uint32_t	Bitmask showing which onboard controllers and sensors are present. Value of 0: not present. Value of 1: present. Indices defined by ENUM MAV_SYS_STATUS_SENSOR (Enum:MAV_SYS_STATUS_SENSOR )
            onboard_control_sensors_enabled	uint32_t	Bitmask showing which onboard controllers and sensors are enabled: Value of 0: not enabled. Value of 1: enabled. Indices defined by ENUM MAV_SYS_STATUS_SENSOR (Enum:MAV_SYS_STATUS_SENSOR )
            onboard_control_sensors_health	uint32_t	Bitmask showing which onboard controllers and sensors are operational or have an error: Value of 0: not enabled. Value of 1: enabled. Indices defined by ENUM MAV_SYS_STATUS_SENSOR (Enum:MAV_SYS_STATUS_SENSOR )
            load	uint16_t	Maximum usage in percent of the mainloop time, (0%: 0, 100%: 1000) should be always below 1000 (Units: d%)
            voltage_battery	uint16_t	Battery voltage, in millivolts (1 = 1 millivolt) (Units: mV)
            current_battery	int16_t	Battery current, in 10*milliamperes (1 = 10 milliampere), -1: autopilot does not measure the current (Units: cA)
            battery_remaining	int8_t	Remaining battery energy: (0%: 0, 100%: 100), -1: autopilot estimate the remaining battery (Units: %)
            drop_rate_comm	uint16_t	Communication drops in percent, (0%: 0, 100%: 10'000), (UART, I2C, SPI, CAN), dropped packets on all links (packets that were corrupted on reception on the MAV) (Units: c%)
            errors_comm	uint16_t	Communication errors (UART, I2C, SPI, CAN), dropped packets on all links (packets that were corrupted on reception on the MAV)
            errors_count1	uint16_t	Autopilot-specific errors
            errors_count2	uint16_t	Autopilot-specific errors
            errors_count3	uint16_t	Autopilot-specific errors
            errors_count4	uint16_t	Autopilot-specific errors
         */

    });

    // 2 SYSTEM_TIME
    mavlink.on("SYSTEM_TIME", function(message, fields) {
        mav_data['telem_1hz'].time_u = fields.time_unix_usec;
        mav_data['telem_1hz'].time_b = Math.round(fields.time_boot_ms/1000);
    });

    // TODO 22
    mavlink.on("PARAM_VALUE", function(message, fields) {

        /*
        param_id	char[16]	Onboard parameter id, terminated by NULL if the length is less than 16
        human-readable chars and WITHOUT null termination (NULL) byte if the length is exactly 16 chars -
        applications have to provide 16+1 bytes storage if the ID is stored as string

        param_value	float	Onboard parameter value
        param_type	uint8_t	Onboard parameter type: see the MAV_PARAM_TYPE enum for supported data types. (Enum:MAV_PARAM_TYPE )
        param_count	uint16_t	Total number of onboard parameters
        param_index	uint16_t	Index of this onboard parameter

        1	MAV_PARAM_TYPE_UINT8	8-bit unsigned integer
        2	MAV_PARAM_TYPE_INT8	8-bit signed integer
        3	MAV_PARAM_TYPE_UINT16	16-bit unsigned integer
        4	MAV_PARAM_TYPE_INT16	16-bit signed integer
        5	MAV_PARAM_TYPE_UINT32	32-bit unsigned integer
        6	MAV_PARAM_TYPE_INT32	32-bit signed integer
        7	MAV_PARAM_TYPE_UINT64	64-bit unsigned integer
        8	MAV_PARAM_TYPE_INT64	64-bit signed integer
        9	MAV_PARAM_TYPE_REAL32	32-bit floating-point
        10	MAV_PARAM_TYPE_REAL64	64-bit floating-point
         */

    });

    // 24 GPS_RAW_INT
    mavlink.on("GPS_RAW_INT", function(message, fields) {
        mav_data['telem_1hz'].gps_fix = fields.fix_type;
        mav_data['telem_1hz'].lat = fields.lat/10000000;
        mav_data['telem_1hz'].lon = fields.lon/10000000;
        mav_data['telem_1hz'].alt = fields.alt/1000;
        mav_data['telem_1hz'].gps_speed = fields.vel/100; // GPS ground speed (m/s * 100). If unknown, set to: UINT16_MAX (Units: cm/s)
        mav_data['telem_1hz'].gps_cog = fields.cog;
        mav_data['telem_1hz'].sats = fields.satellites_visible;
    });

    // TODO 25
    mavlink.on("GPS_STATUS", function(message, fields) {

    });

    // TODO 26
    mavlink.on("SCALED_IMU", function(message, fields) {

    });

    // TODO 27
    mavlink.on("RAW_IMU", function(message, fields) {

    });

    // 29 SCALED_PRESSURE
    mavlink.on("SCALED_PRESSURE", function(message, fields) {
        mav_data['telem_1hz'].press_a = Math.round(fields.press_abs);
        mav_data['telem_1hz'].press_d = fields.press_diff;
        mav_data['telem_1hz'].temp = Math.round(fields.temperature/100); // Temperature measurement (0.01 degrees celsius) (Units: cdegC)
    });

    // 30 ATTITUDE
    mavlink.on("ATTITUDE", function(message, fields) {
        const pi = Math.PI;
        mav_data['telem_10hz'].r = Math.round(fields.roll * (180/pi)); // Roll angle (rad, -pi..+pi) (Units: rad)
        mav_data['telem_10hz'].p = Math.round(fields.pitch * (180/pi));
        mav_data['telem_10hz'].y = Math.round(fields.yaw * (180/pi));
        mav_data['telem_10hz'].rs = Math.round(fields.rollspeed * (180/pi)); // Roll angular speed (rad/s) (Units: rad/s)
        mav_data['telem_10hz'].ps = Math.round(fields.pitchspeed * (180/pi));
        mav_data['telem_10hz'].ys = Math.round(fields.yawspeed * (180/pi));
    });

    // TODO 32
    mavlink.on("LOCAL_POSITION_NED", function(message, fields) {

    });

    // 33 GLOBAL_POSITION_INT
    mavlink.on("GLOBAL_POSITION_INT", function(message, fields) {
        mav_data['telem_1hz'].pos_lat = fields.lat/10000000; // Latitude, expressed as degrees * 1E7 (Units: degE7)
        mav_data['telem_1hz'].pos_lon = fields.lon/10000000;
        mav_data['telem_1hz'].pos_alt = fields.alt/1000; // Altitude in meters, expressed as * 1000 (millimeters), AMSL (not WGS84 - note that virtually all GPS modules provide the AMSL as well) (Units: mm)
        mav_data['telem_1hz'].pos_rel_alt = fields.relative_alt/1000; // Altitude above ground in meters, expressed as * 1000 (millimeters) (Units: mm)
        mav_data['telem_1hz'].pos_vx = fields.vx/100; // Ground X Speed (Latitude, positive north), expressed as m/s * 100 (Units: cm/s)
        mav_data['telem_1hz'].pos_vy = fields.vy/100;
        mav_data['telem_1hz'].pos_vz = fields.vz/100;
        mav_data['telem_1hz'].pos_hdg = Math.round(fields.hdg/100); // Vehicle heading (yaw angle) in degrees * 100, 0.0..359.99 degrees. If unknown, set to: UINT16_MAX (Units: cdeg)
    });

    // TODO 35
    mavlink.on("RC_CHANNELS_RAW", function(message, fields) {

    });

    // TODO 39
    mavlink.on("MISSION_ITEM", function(message, fields) {

    });

    // TODO 42
    mavlink.on("MISSION_CURRENT", function(message, fields) {

    });

    // TODO 44
    mavlink.on("MISSION_COUNT", function(message, fields) {

    });

    // TODO 46
    mavlink.on("MISSION_ITEM_REACHED", function(message, fields) {
        /*
            A certain mission item has been reached.
            The system will either hold this position (or circle on the orbit) or
            (if the autocontinue on the WP was set) continue to the next waypoint.

            seq	uint16_t	Sequence
         */
    });

    // TODO 47
    mavlink.on("MISSION_ACK", function(message, fields) {
        /*
            Ack message during waypoint handling. The type field states if this message is a positive ack (type=0)
            or if an error happened (type=non-zero).

            target_system	uint8_t	System ID
            target_component	uint8_t	Component ID
            type	uint8_t	See MAV_MISSION_RESULT enum (Enum:MAV_MISSION_RESULT )
            mission_type **	uint8_t	Mission type, see MAV_MISSION_TYPE (Enum:MAV_MISSION_TYPE )
         */

    });

    // TODO !! 62
    mavlink.on("NAV_CONTROLLER_OUTPUT", function(message, fields) {
        /*
        nav_roll	float	Current desired roll in degrees (Units: deg)
        nav_pitch	float	Current desired pitch in degrees (Units: deg)
        nav_bearing	int16_t	Current desired heading in degrees (Units: deg)
        target_bearing	int16_t	Bearing to current waypoint/target in degrees (Units: deg)
        wp_dist	uint16_t	Distance to active waypoint in meters (Units: m)
        alt_error	float	Current altitude error in meters (Units: m)
        aspd_error	float	Current airspeed error in meters/second (Units: m/s)
        xtrack_error	float	Current crosstrack error on x-y plane in meters (Units: m)
         */
    });

    // TODO 73
    mavlink.on("MISSION_ITEM_INT", function(message, fields) {
        /*
            Message encoding a mission item. This message is emitted to announce the presence of a mission item and to set a mission item on the system. The mission item can be either in x, y, z meters (type: LOCAL) or x:lat, y:lon, z:altitude. Local frame is Z-down, right handed (NED), global frame is Z-up, right handed (ENU). See alsohttp://qgroundcontrol.org/mavlink/waypoint_protocol.+

                Field Name	Type	Description
                target_system	uint8_t	System ID
                target_component	uint8_t	Component ID
                seq	uint16_t	Waypoint ID (sequence number). Starts at zero. Increases monotonically for each waypoint, no gaps in the sequence (0,1,2,3,4).
                frame	uint8_t	The coordinate system of the waypoint. see MAV_FRAME in mavlink_types.h (Enum:MAV_FRAME )
                command	uint16_t	The scheduled action for the waypoint. see MAV_CMD in common.xml MAVLink specs (Enum:MAV_CMD )
                current	uint8_t	false:0, true:1
                autocontinue	uint8_t	autocontinue to next wp
                param1	float	PARAM1, see MAV_CMD enum
                param2	float	PARAM2, see MAV_CMD enum
                param3	float	PARAM3, see MAV_CMD enum
                param4	float	PARAM4, see MAV_CMD enum
                x	int32_t	PARAM5 / local: x position in meters * 1e4, global: latitude in degrees * 10^7
                y	int32_t	PARAM6 / y position: local: x position in meters * 1e4, global: longitude in degrees *10^7
                z	float	PARAM7 / z position: global: altitude in meters (relative or absolute, depending on frame.
                mission_type **	uint8_t	Mission type, see MAV_MISSION_TYPE (Enum:MAV_MISSION_TYPE )

         */
    });

    // TODO 74
    mavlink.on("VFR_HUD", function(message, fields) {
        /*
            Metrics typically displayed on a HUD for fixed wing aircraft
                Field Name	Type	Description
                airspeed	float	Current airspeed in m/s (Units: m/s)
                groundspeed	float	Current ground speed in m/s (Units: m/s)
                heading	int16_t	Current heading in degrees, in compass units (0..360, 0=north) (Units: deg)
                throttle	uint16_t	Current throttle setting in integer percent, 0 to 100 (Units: %)
                alt	float	Current altitude (MSL), in meters (Units: m)
                climb	float	Current climb rate in meters/second (Units: m/s)
         */
    });

    // 77
    // in class Drone

    // TODO !! 125
    mavlink.on("POWER_STATUS", function(message, fields) {
        /*
        Vcc	uint16_t	5V rail voltage in millivolts (Units: mV)
        Vservo	uint16_t	servo rail voltage in millivolts (Units: mV)
        flags	uint16_t	power supply status flags (see MAV_POWER_STATUS enum) (Enum:MAV_POWER_STATUS )
         */
    });

    // TODO 141
    mavlink.on("ALTITUDE", function(message, fields) {

    });

    // TODO 148
    mavlink.on("AUTOPILOT_VERSION", function(message, fields) {
        console.log('48 AUTOPILOT_VERSION');
        console.log(fields);
    });

    // TODO 149
    mavlink.on("LANDING_TARGET", function(message, fields) {

    });

    // TODO 152
    mavlink.on("MEMINFO", function(message, fields) {

    });

    // TODO 163
    mavlink.on("AHRS", function(message, fields) {

    });

    // TODO 165
    mavlink.on("HWSTATUS", function(message, fields) {

    });

    // TODO 241
    mavlink.on("VIBRATION", function(message, fields) {
        /*
        time_usec	uint64_t	Timestamp (micros since boot or Unix epoch) (Units: us)
        vibration_x	float	Vibration levels on X-axis
        vibration_y	float	Vibration levels on Y-axis
        vibration_z	float	Vibration levels on Z-axis
        clipping_0	uint32_t	first accelerometer clipping count
        clipping_1	uint32_t	second accelerometer clipping count
        clipping_2	uint32_t	third accelerometer clipping count
         */
    });

    // TODO !! 242
    mavlink.on("HOME_POSITION", function(message, fields) {
        /*
        This message can be requested by sending the MAV_CMD_GET_HOME_POSITION command.
        The position the system will return to and land on. The position is set automatically by
        the system during the takeoff in case it was not explicitely set by the operator before or after.
        The position the system will return to and land on. The global and local positions encode
        the position in the respective coordinate frames, while the q parameter encodes the orientation of
        the surface. Under normal conditions it describes the heading and terrain slope, which can be used
        by the aircraft to adjust the approach. The approach 3D vector describes the point to which the system
        should fly in normal flight mode and then perform a landing sequence along the vector.

        latitude	int32_t	Latitude (WGS84), in degrees * 1E7 (Units: degE7)
        longitude	int32_t	Longitude (WGS84, in degrees * 1E7 (Units: degE7)
        altitude	int32_t	Altitude (AMSL), in meters * 1000 (positive for up) (Units: mm)
        x	float	Local X position of this position in the local coordinate frame (Units: m)
        y	float	Local Y position of this position in the local coordinate frame (Units: m)
        z	float	Local Z position of this position in the local coordinate frame (Units: m)
        q	float[4]	World to surface normal and heading transformation of the takeoff position. Used to indicate the heading and slope of the ground
        approach_x	float	Local X position of the end of the approach vector. Multicopters should set this position based on their takeoff path. Grass-landing fixed wing aircraft should set it the same way as multicopters. Runway-landing fixed wing aircraft should set it to the opposite direction of the takeoff, assuming the takeoff happened from the threshold / touchdown zone. (Units: m)
        approach_y	float	Local Y position of the end of the approach vector. Multicopters should set this position based on their takeoff path. Grass-landing fixed wing aircraft should set it the same way as multicopters. Runway-landing fixed wing aircraft should set it to the opposite direction of the takeoff, assuming the takeoff happened from the threshold / touchdown zone. (Units: m)
        approach_z	float	Local Z position of the end of the approach vector. Multicopters should set this position based on their takeoff path. Grass-landing fixed wing aircraft should set it the same way as multicopters. Runway-landing fixed wing aircraft should set it to the opposite direction of the takeoff, assuming the takeoff happened from the threshold / touchdown zone. (Units: m)
        time_usec **	uint64_t	Timestamp (microseconds since UNIX epoch or microseconds since system boot) (Units: us)
         */

        console.log('242 HOME_POSITION');
        console.log(fields);

    });

    // TODO 244
    mavlink.on("MESSAGE_INTERVAL", function(message, fields) {
        /*
            message_id	uint16_t	The ID of the requested MAVLink message. v1.0 is limited to 254 messages.
            interval_us	int32_t	The interval between two messages, in microseconds. A value of -1 indicates this stream is disabled, 0 indicates it is not available, > 0 indicates the interval at which it is sent. (Units: us)
         */
    });


};

module.exports = mav_json;