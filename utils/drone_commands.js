module.exports = function(mavlink, io_client) {

    let io_emit = function(msg){
        io_client.to('drone').emit('mav', msg.buffer);
    };


    return {

        takeoff: function (params) {
            console.log('take off');
            //console.log(udp_out_msgs);

            mavlink.createMessage('COMMAND_LONG', {
                target_system: mavlink.sysid
                ,target_component: mavlink.compid
                ,command: 22 // MAV_CMD -> MAV_CMD_NAV_TAKEOFF
                ,confirmation: 0
                ,param1: 0
                ,param2: ''
                ,param3: ''
                ,param4: 0
                ,param5: ''
                ,param6: ''
                ,param7: params.alt

            }, io_emit );

        }

        ,land: function () {
            console.log('land');
        }

        ,switch_relay: function (params) {
            console.log('switch relay');
            //console.log(udp_out_msgs);

            if( !params.relay || !params.switch ) return;

            mavlink.createMessage('COMMAND_LONG', {
                target_system: mavlink.sysid
                ,target_component: mavlink.compid
                ,command: 181 // MAV_CMD -> MAV_CMD_DO_SET_RELAY
                ,confirmation: 0
                ,param1: parseInt(params.relay)
                ,param2: params.switch === 'on' ? 1 : 0
                ,param3: null
                ,param4: null
                ,param5: null
                ,param6: null
                ,param7: null

            }, io_emit );

        }

    };
};


/*



COMMAND_LONG ( #76 )

Send a command with up to seven parameters to the MAV
Field Name	Type	Description

target_system	uint8_t	System which should execute the command
target_component	uint8_t	Component which should execute the command, 0 for all components
command	uint16_t	Command ID, as defined by MAV_CMD enum. (Enum:MAV_CMD )
confirmation	uint8_t	0: First transmission of this command. 1-255: Confirmation transmissions (e.g. for kill command)
param1	float	Parameter 1, as defined by MAV_CMD enum.
param2	float	Parameter 2, as defined by MAV_CMD enum.
param3	float	Parameter 3, as defined by MAV_CMD enum.
param4	float	Parameter 4, as defined by MAV_CMD enum.
param5	float	Parameter 5, as defined by MAV_CMD enum.
param6	float	Parameter 6, as defined by MAV_CMD enum.
param7	float	Parameter 7, as defined by MAV_CMD enum.


22	MAV_CMD_NAV_TAKEOFF	Takeoff from ground / hand
Mission Param #1	Minimum pitch (if airspeed sensor present), desired pitch without sensor
Mission Param #2	Empty
Mission Param #3	Empty
Mission Param #4	Yaw angle (if magnetometer present), ignored without magnetometer. NaN for unchanged.
Mission Param #5	Latitude
Mission Param #6	Longitude
Mission Param #7	Altitude



MANUAL_CONTROL ( #69 )

This message provides an API for manually controlling the vehicle using standard joystick axes nomenclature, along with a joystick-like input device. Unused axes can be disabled an buttons are also transmit as boolean values of their
Field Name	Type	Description
target	uint8_t	The system to be controlled.
x	int16_t	X-axis, normalized to the range [-1000,1000]. A value of INT16_MAX indicates that this axis is invalid. Generally corresponds to forward(1000)-backward(-1000) movement on a joystick and the pitch of a vehicle.
y	int16_t	Y-axis, normalized to the range [-1000,1000]. A value of INT16_MAX indicates that this axis is invalid. Generally corresponds to left(-1000)-right(1000) movement on a joystick and the roll of a vehicle.
z	int16_t	Z-axis, normalized to the range [-1000,1000]. A value of INT16_MAX indicates that this axis is invalid. Generally corresponds to a separate slider movement with maximum being 1000 and minimum being -1000 on a joystick and the thrust of a vehicle. Positive values are positive thrust, negative values are negative thrust.
r	int16_t	R-axis, normalized to the range [-1000,1000]. A value of INT16_MAX indicates that this axis is invalid. Generally corresponds to a twisting of the joystick, with counter-clockwise being 1000 and clockwise being -1000, and the yaw of a vehicle.
buttons	uint16_t	A bitfield corresponding to the joystick buttons' current state, 1 for pressed, 0 for released. The lowest bit corresponds to Button 1.


 */




/*

    // Первое сообщение - запрос всех параметров
    drone_mavlink.createMessage("PARAM_REQUEST_LIST", {
        'target_system': drone.sys_id
        ,'target_component': drone.comp_id
    }, send_to_board);


 */