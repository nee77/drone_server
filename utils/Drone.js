// Node-mavlink
const MAVlink = require('mavlink');

const mav_json = require('./mav_json');

let drones_list = {};

class Drone {
    static get(drone_id){
        if( drones_list['drone_' + drone_id] ) return drones_list['drone_' + drone_id];
        else return null;
    }

    //static

    constructor(io_client, params){

        this.io_client = io_client;
        this.io_channel = 'drone_' + params.id;

        this.id = params.id;

        drones_list['drone_' + this.id] = this;

        // Инициализация MAVLink
        this.mavlink = new MAVlink(params.sysid, params.compid, params.mav_v, params.mav_msg_def);
        this.mavlink_ready = false;

        this.mav_data = {};

        this.send_interval_telem_1hz = null;
        this.send_interval_telem_10hz = null;
        this.send_interval_info = null;
        //this.telemetry_on

        const _this = this;

        // Подключение к каналу
        io_client.join(_this.io_channel);


        _this.mavlink.on("ready", function() {
            _this.mavlink_ready = true;

            // Парсим сообщения от дрона
            io_client.on('mav', function(message){
                // Парсим сообщение
                _this.mavlink.parse(message);

                //redisClient.set('last_telem_drone_' + drone_id, (new Date()).getTime());
            });

        });

        // Преобразование сообщений mavlink в json и сохранение в переменной
        mav_json(_this.mavlink, _this.mav_data);

    }

    start_telemetry(gcs_id){ console.log('start telemetry from ' + this.id + ' to ' + gcs_id);
        const _this = this;

        // Сообщения со статусом отправляем как только придут
        _this.mavlink.on("STATUSTEXT", function(message, fields) {
            /*
            severity	uint8_t	Severity of status. Relies on the definitions within RFC-5424. See enum MAV_SEVERITY. (Enum:MAV_SEVERITY )
            text	char[50]	Status text message, without null termination character

                0	MAV_SEVERITY_EMERGENCY	System is unusable. This is a "panic" condition.
                1	MAV_SEVERITY_ALERT	Action should be taken immediately. Indicates error in non-critical systems.
                2	MAV_SEVERITY_CRITICAL	Action must be taken immediately. Indicates failure in a primary system.
                3	MAV_SEVERITY_ERROR	Indicates an error in secondary/redundant systems.
                4	MAV_SEVERITY_WARNING	Indicates about a possible future error if this is not resolved within a given timeframe. Example would be a low battery warning.
                5	MAV_SEVERITY_NOTICE	An unusual event has occured, though not an error condition. This should be investigated for the root cause.
                6	MAV_SEVERITY_INFO	Normal operational messages. Useful for logging. No action is required for these messages.
                7	MAV_SEVERITY_DEBUG	Useful non-operational messages that can assist in debugging. These should not occur during normal operation.

             */

            _this.io_client.to(gcs_id).emit('status_text_' + _this.id, fields);

        });

        _this.mavlink.on("COMMAND_ACK", function(message, fields) {
            /*
            command	uint16_t	Command ID, as defined by MAV_CMD enum. (Enum:MAV_CMD )
            result	uint8_t	See MAV_RESULT enum (Enum:MAV_RESULT )
            progress **	uint8_t	WIP: Also used as result_param1, it can be set with a enum containing the errors reasons of why the command was denied or the progress percentage or 255 if unknown the progress when result is MAV_RESULT_IN_PROGRESS.
            result_param2 **	int32_t	WIP: Additional parameter of the result, example: which parameter of MAV_CMD_NAV_WAYPOINT caused it to be denied.
            target_system **	uint8_t	WIP: System which requested the command to be executed
            target_component **	uint8_t	WIP: Component which requested the command to be executed

            ** MAV_CMD


            ** MAV_RESULT


             */

            // Отправляем в GCS подтверждение исполнения команды и ее результат
            _this.io_client.to(gcs_id).emit('com_ack_' + _this.id, fields);

        });

        _this.mavlink.on("PING", function(message, fields) {
            /*
                seq	uint32_t		PING sequence
                target_system	uint8_t		0: request ping from all receiving systems, if greater than 0: message is a ping response and number is the system id of the requesting system
                target_component	uint8_t		0: request ping from all receiving components, if greater than 0: message is a ping response and number is the system id of the requesting system
             */

            // Отправляем в GCS подтверждение исполнения команды и ее результат
            _this.io_client.to(gcs_id).emit('ping_' + _this.id, fields);

        });


        // Отправляем телеметрию в канал с заданной частотой
        _this.send_interval_telem_1hz = setInterval(function(){

            if( _this.mav_data.telem_1hz ) _this.io_client.to(gcs_id).emit('telem_1hz_' + _this.id, _this.mav_data.telem_1hz);

        }, 1000);
        _this.send_interval_telem_10hz = setInterval(function(){

            if( _this.mav_data.telem_10hz ) _this.io_client.to(gcs_id).emit('telem_10hz_' + _this.id, _this.mav_data.telem_10hz);

        }, 50);
        _this.send_interval_info = setInterval(function(){

            if( _this.mav_data.info ) _this.io_client.to(gcs_id).emit('info_' + _this.id, _this.mav_data.info);

        }, 10000);

    }

    stop_telemetry(){
        if( this.send_interval_telem_1hz ) clearInterval(this.send_interval_telem_1hz);
        if( this.send_interval_telem_10hz ) clearInterval(this.send_interval_telem_10hz);
        if( this.send_interval_info ) clearInterval(this.send_interval_info);

        this.send_interval_telem_1hz = null;
        this.send_interval_telem_1hz = null;
        this.send_interval_info = null;
    }

    command(data){
        const _this = this;

        // Отправка буфера на борт
        let io_emit = function(msg){
            _this.io_client.emit('mav', msg.buffer);
        };

        //console.log('Drone.command');
        //console.log(data.command);


        if( 'info' === data.command ){
            if( _this.mav_data.info ) _this.io_client.to('gcs').emit('info_' + _this.id, _this.mav_data.info);
        }

        // Установка режима
        else if( 'set_mode' === data.command ){

            _this.mavlink.createMessage('SET_MODE', {
                target_system: _this.mavlink.sysid
                ,base_mode: data.params.base_mode
                ,custom_mode: data.params.custom_mode

            }, io_emit );

            //console.log('DO SET_MODE');
            //console.log(data.params);

        }

        // Команда Взлет
        else if( 'takeoff' === data.command ){
            _this.mavlink.createMessage('COMMAND_LONG', {
                target_system: _this.mavlink.sysid
                ,target_component: _this.mavlink.compid
                ,command: 22 // MAV_CMD -> MAV_CMD_NAV_TAKEOFF
                ,confirmation: 0
                ,param1: 0
                ,param2: ''
                ,param3: ''
                ,param4: 0
                ,param5: ''
                ,param6: ''
                ,param7: data.params.alt

            }, io_emit );
        }

        // Управление реле
        else if( 'switch_relay' === data.command ) {

            if( !data.params.relay || !data.params.switch ) return;

            _this.mavlink.createMessage('COMMAND_LONG', {
                target_system: _this.mavlink.sysid
                ,target_component: _this.mavlink.compid
                ,command: 181 // MAV_CMD -> MAV_CMD_DO_SET_RELAY
                ,confirmation: 0
                ,param1: parseInt(data.params.relay)
                ,param2: data.params.switch === 'on' ? 1 : 0
                ,param3: null
                ,param4: null
                ,param5: null
                ,param6: null
                ,param7: null

            }, io_emit );

        }

        // Пинг
        else if( 'ping' === data.command ){

        }

    }

    disconnect(){
        this.stop_telemetry();

        this.mavlink = null;
    }

}

module.exports = Drone;


/*

// Отправка исходящего сообщения на борт
    const send_to_board = function(message){
        io_client.emit('fromserver', message.buffer);
    };
    // Создаем исходящее сообщение и отправляем его на борт
    drone_mavlink.createMessage(com_data.command, com_data.params, send_to_board);




    // Первое сообщение - запрос всех параметров
    drone_mavlink.createMessage("PARAM_REQUEST_LIST", {
        'target_system': drone.sys_id
        ,'target_component': drone.comp_id
    }, send_to_board);


 */

