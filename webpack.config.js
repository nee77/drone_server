var path = require("path");
var webpack = require("webpack");

module.exports = function(env) {

	var pack = require("./package.json");
	var ExtractTextPlugin = require("extract-text-webpack-plugin");
	var production = !!(env && env.production === "true");
	var babelSettings = {
		extends: path.join(__dirname, '/.babelrc')
	};

	var config = {
		entry: "./web-ui/sources/app.js",
		output: {
			path: path.join(__dirname, "web-ui/codebase"),
			publicPath:"web-ui/codebase/",
			filename: "app.js"
		},
		devtool: "inline-source-map",
		devServer: {
            host: '127.0.0.1',
			port: '8081',
            disableHostCheck: true
			,proxy: {
                '/api': {
                    target: 'http://localhost:8091',
                    secure: false
                }
                ,'/socket.io': {
                    target: 'http://localhost:8091',
                    ws: true
                }
            }
        },
		module: {
			rules: [
				{
					test: /\.js$/,
					loader: "babel-loader?" + JSON.stringify(babelSettings)
				},
				{
					test: /\.(svg|png|jpg|gif)$/,
					loader: "url-loader?limit=25000"
				},
				{
					test: /\.(less|css)$/,
					loader: ExtractTextPlugin.extract("css-loader!less-loader")
				}
			]
		},
		resolve: {
			extensions: [".js"],
			modules: ["./web-ui/sources", "node_modules"],
			alias:{
				"jet-views":path.resolve(__dirname, "web-ui/sources/views"),
				"jet-locales":path.resolve(__dirname, "web-ui/sources/locales")
			}
		},
		plugins: [
			new ExtractTextPlugin("./app.css"),
			new webpack.DefinePlugin({
				VERSION: `"${pack.version}"`,
				APPNAME: `"${pack.name}"`,
				PRODUCTION : production
			})
		]
	};

	if (production) {
		config.plugins.push(
			new  webpack.optimize.UglifyJsPlugin({
				test: /\.js$/
			})
		);
	}

	return config;
}